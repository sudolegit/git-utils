#!/bin/bash
####################################################################################################
##  @file
##  
##  @brief          Library file providing helper functions related to navigating a git repository's 
##                  history.
##  
##  @depends
##                  - BASH      [4.0++]
##                  - git       [2.11.0++]
##  
##  @version        1.0.0
####################################################################################################


####################################################################################################
##  @fn             walk_branch()
##  
##  @brief          Move branch pointer to the target commit specified. The target commit is 
##                  relative to the current checkout point and is determined from the parameters 
##                  provided.
##  
##  @details        Function first validates input parameters and ensures the current repository is:
##                      -   Checked out to a local branch.
##                      -   In a clean enough state (untracked changes are fine).
##                  
##                  @par Determines the 'SHA' to checkout by first determining the SHA for the 
##                  active commit and a list of all SHAs on the active branch. The active branch is 
##                  then reset hard to the SHA at an offset of 'stride_length' to the current 
##                  commit.
##  
##  @note           If there is ambiguity for where to step to when walking the active branch, the 
##                  user is prompted to make a selection. Once a valid selection is made, walking 
##                  along the active branch continues.
##  
##  @note           Commits deemed 'accessible' [from the current SHA] are limited to those on 
##                  remote and local branches matching the name of the active branch unless 
##                  'flag_all_branches' is set to true (then all branches on all remotes are 
##                  included).
##                  
##  @param[in]      direction           Direction to head. Must be one of the following values:
##                                          -   'up'    Move towards more recent development
##                                          -   'down'  Move towards older development
##  @param[in]      stride_length       How many commits to move. Must be a number greater than or 
##                                      equal to 1.
##  @param[in]      flag_all_branches   Flag indicating if all branches should be searched. When 
##                                      set to true, all branches will be searched. This allows 
##                                      walking a branch pointer above where a branch currently 
##                                      ends and navigating to commits which are still accessible 
##                                      relative to the current SHA but are not on the current 
##                                      branch.
##                                      
##  
##  @retval         0                   Successfully switched to requested commit.
##  @retval         1                   No commit point available to switch to.
##  @retval         2                   Value provided for 'direction' is invalid.
##  @retval         3                   Value provided for 'stride_length' is invalid.
##  @retval         4                   Repository is in a 'detached HEAD' state.
##  @retval         5                   Repository has uncommitted changes.
##  @retval         6                   Reached end of path before stride length met.
####################################################################################################
function walk_branch() {
    
    local direction="${1:-NULL}"                                                # Indicates if the walk should be 'up' or 'down' (relative to the current commit).
    local stride_length="${2:-NULL}"                                            # Indicates how many commits we should walk.
    local flag_all_branches="${3:-0}"                                           # Indicates if all branches should be searched.
    local re='^[0-9]+$'                                                         # Used to validate the 'stride_length' provided is numerical.
    
    if [[ "${direction}" != "up" ]] && [[ "${direction}" != "down" ]]; then
        print_verbose_message "Error:  Provided value of '${direction}' for walk direction is invalid"
        return 2
    elif ! [[ ${stride_length} =~ $re ]] || [ ${stride_length} -le 0 ]; then
        print_verbose_message "Error:  Provided value of '${stride_length}' for stride_length is invalid"
        return 3
    elif [ $(repository_head_is_detached) -eq 1 ]; then
        print_verbose_message "Error:  Repository is in a 'detached HEAD' state"
        return 4
    elif [ $(repository_state_is_clean_enough) -eq 0 ]; then
        print_verbose_message "Error:  Repository has one or more uncommitted changes"
        return 5
    fi
    
    # Determine the branch and SHA for the current commit
    branch="$(git rev-parse --abbrev-ref HEAD)"
    sha=$(git rev-parse HEAD)
    
    print_verbose_message "Staring walk of branch ${branch} @ SHA-1 ${sha}"
    
    # Determine rev-list filter to use. Note that a string match is intentionally used to avoid 
    # issues with formatting for the 'flag_all_branches' parameter (any non-zero value == yes).
    if [[ "${flag_all_branches}" == "0" ]]; then
        rev_list_filter="--glob=*/${branch}"
    else
        rev_list_filter="--all"
    fi
    
    step_count=0
    while [ ${step_count} -lt ${stride_length} ]; do
        
        step_count=$((step_count + 1))
        
        # Get a space separated list of all SHAs accessible from the present 'SHA-1' in the 
        # direction we are walking the tree.
        if [[ "${direction}" == "up" ]]; then
            shas=($(git rev-list ${rev_list_filter} --children | grep "^${sha}" | cut -d ' ' -s -f2-))
        else
            shas=($(git rev-list ${rev_list_filter} --parents  | grep "^${sha}" | cut -d ' ' -s -f2-))
        fi

        if [ ${#shas[@]} -eq 0 ]; then
            print_message "Error:  Reached dead end while walking branch."
            return 6
        elif [ ${#shas[@]} -eq 1 ]; then
            # Update sha for next iteration in loop
            sha="${shas}"
            print_verbose_message "Stepping to SHA-1 ${sha}"
        else
            # Ask user to choose
            echo -e "\nBranch point reached. The following commits are accessible:"
            selection=0
            for entry in "${shas[@]}"; do
                selection=$((selection + 1))
                commit_message="$(git show ${entry} --oneline --format=%s | head -n 1)"
                echo "  [${selection}] ${commit_message}"
            done
            
            echo ""
            while true; do
                read -p "  Please enter in the number of the commit you would like to walk to: "  user_selection
                if [[ ${user_selection} =~ $re ]] && [ ${user_selection} -ge 1 ] && [ ${user_selection} -le ${#shas[@]} ]; then
                    break
                fi
            done
            echo ""
            
            sha=${shas[$((user_selection - 1))]}
            
        fi
        
    done
    
    print_message "Resetting current branch to SHA '${sha}'"
    
    if [ $(verbose_mode_is_enabled) -eq 1 ]; then
        git reset --hard ${sha}
    else
        git reset --hard ${sha} > /dev/null 2>&1
    fi
    
    return 0
    
}


