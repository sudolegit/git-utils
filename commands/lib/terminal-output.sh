#!/bin/bash
####################################################################################################
##  @file
##  
##  @brief          Library file providing helper functions related to terminal output.
##  
##  @depends
##                  - BASH      [4.0++]
##  
##  @version        1.0.0
####################################################################################################


####################################################################################################
##  @fn             print_message()
##  
##  @brief          Print message provided to the terminal.
##  
##  @details        Simple wrapper to echo that prints all parameters as received. Creating as a 
##                  method of future proofing terminal output.
##  
##  @param[in]      all                 Message to print (all parameters will be printed).
####################################################################################################
function print_message() {
    echo "${@}"
}


####################################################################################################
##  @fn             print_verbose_message()
##  
##  @brief          Print message provided to the terminal when in verbose mode.
##  
##  @details        Invokes the 'print_message()' function when verbose mode is enabled (otherwise 
##                  exits without printing).
##  
##  @param[in]      all                 Message to print (all parameters will be printed).
####################################################################################################
function print_verbose_message() {
    if [ $(verbose_mode_is_enabled) -eq 1 ]; then
        print_message "${@}"
    fi
}


