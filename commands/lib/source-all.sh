#!/bin/bash
####################################################################################################
##  @file
##  
##  @brief          Main script used to import all helper git BASH library scripts.
##  
##  @details        Determines the directory this script resides in (actual folder/following 
##                  symlinks) and then sources all files found in this directory that end in the 
##                  file extension '*.sh'.
##  
##  @depends          
##                  - BASH 4.0++
##  
##  @version        - 1.0.0
####################################################################################################


#===================================================================================================
# @note             Determine root location [directory] for the repository installation.
# 
# @warning          This code snippet is used in a variety of files. To avoid any variable 
#                   collisions (since this script is meant to be pulled into other files) we prepend 
#                   an underscore to the variables in this next section.
#---------------------------------------------------------------------------------------------------
# Resolve '$_PATH_TO_FILE' until the file is no longer a symlink.
_PATH_TO_FILE="${BASH_SOURCE[0]}"
while [ -h "$_PATH_TO_FILE" ]; do
	# Grab next component of _PATH_TO_FILE 
	_SCRIPT_DIRECTORY="$( cd -P "$( dirname "$_PATH_TO_FILE" )" && pwd )"
	_PATH_TO_FILE="$(readlink "$_PATH_TO_FILE")"
	
	# If $_PATH_TO_FILE was a relative symlink, we need to resolve it relative to the path where the 
    # symlink file was located.
	[[ $_PATH_TO_FILE != /* ]] && _PATH_TO_FILE="$_SCRIPT_DIRECTORY/$_PATH_TO_FILE"
done

# Define base script directory and root directory for installation.
_SCRIPT_DIRECTORY="$( cd -P "$( dirname "$_PATH_TO_FILE" )" && pwd )"


#===================================================================================================
# @brief            Source [load in] all bash scripts in this library directory.
#---------------------------------------------------------------------------------------------------
for file in $(find ${_SCRIPT_DIRECTORY} -type f | grep "\.sh" | grep -v $(basename ${BASH_SOURCE[0]})); do
    source "${file}"
done


