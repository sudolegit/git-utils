#!/bin/bash
####################################################################################################
##  @file
##  
##  @brief          Library file providing helper functions related to interacting with git 
##                  branches. 
##  
##  @depends
##                  - BASH      [4.0++]
##                  - git       [2.11.0++]
##  
##  @version        1.0.0
####################################################################################################


####################################################################################################
##  @fn             local_branch_exists()
##  
##  @brief          Determine if the branch name provided exists on the present list of local 
##                  branches.
##  
##  @details        Uses the git 'branch' command, sed, awk, grep, cut, and wc to determine if the 
##                  branch name provided exists in the present list of local branches.
##  
##  @param[in]      branch              Name of local branch to search for in repository.
##  
##  @retval         0                   Branch exists.
##  @retval         1                   Missing required parameter(s) to function.
##  @retval         2                   Branch does not exist locally.
####################################################################################################
function local_branch_exists() {
    
    local branch="${1:-NULL}"
    
    if [[ "${branch}" == "NULL" ]]; then
        echo 1
        return
    fi
    
    if [ $(git branch -v | sed -e 's/*//g' | awk '{$1=$1}1' | grep -v ^"remotes/" | cut -d' ' -f1 | awk '{$1=$1}1' | grep ^"${branch}"$ | wc -l) -ge 1 ]; then
        echo 0
    else
        echo 2
    fi
}


####################################################################################################
##  @fn             remote_branch_exists()
##  
##  @brief          Determine if the branch name provided exists on the present list of the target  
##                  remote's branches.
##  
##  @details        Verifies the target remote exists and is accessible. If it is, uses the git 
##                  'fetch' command to update the list of remote branches and uses the git 'branch' 
##                  command, sed, awk, grep, cut, and wc to determine if the branch name provided 
##                  exists in the present list of a remote's branches.
##  
##  @param[in]      branch              Name of branch to search for in remote.
##  @param[in]      remote              [Optional] Name of remote to target. Defaults to 'origin' if 
##                                      no value provided.
##  
##  @retval         0                   Branch exists on remote.
##  @retval         1                   Missing required parameter(s) to function.
##  @retval         2                   Remote does not exist.
##  @retval         3                   Remote is not accessible.
##  @retval         4                   Branch does not exist on remote.
####################################################################################################
function remote_branch_exists() {
    
    local branch="${1:-NULL}"
    local remote="${2:-origin}"
    
    if [[ "${branch}" == "NULL" ]]; then
        echo 1
        return
    fi
    
    if ! [ $(remote_exists "${remote}") -eq 0 ]; then
        echo 2
        return
    fi
    
    if ! [ $(remote_accessible "${remote}") -eq 0 ]; then
        echo 3
        return
    fi
    
    git fetch "${remote}"
    
    if [ $(git branch -va | awk '{$1=$1}1' | grep ^"remotes/${remote}/" | cut -d' ' -f1 | awk '{$1=$1}1' | grep "/${branch}"$ | wc -l) -ge 1 ]; then
        echo 0
    else
        echo 4
    fi
}


####################################################################################################
##  @fn             get_name_of_current_branch()
##  
##  @brief          Returns the name of the current branch.
##  
##  @details        Uses the git 'branch' command to list the local branches and determines the one 
##                  flagged with '*' as active. This active branch name is then returned to the 
##                  caller (or an empty string if in a headless state).
##  
##  @retval         [nothing]           Repository is in a headless state.
##  @retval         branch-name         Name of current [active] branch.
####################################################################################################
function get_name_of_current_branch() {
    
    local tmp="$(git branch | grep '*' | sed -e 's/*//g' | awk '{$1=$1}1')"
    
    # Return name of branch so long as not in detached / headless state
    if ! [ $(echo ${tmp} | grep HEAD | grep detached | wc -l) -eq 1 ]; then
        echo "${tmp}"
    fi
}


####################################################################################################
##  @fn             checkout_branch()
##  
##  @brief          Checkout the requested branch.
##  
##  @details        Confirm not already on the branch requested (exit if true). Provided this is not
##                  the case, checks out the branch in one of three conditions:
##                      1.  Local branch instance exists:
##                          -   Checkout existing instance.
##                      2.  No local instance but remote branch exists:
##                          -   Checkout instance at point of remote branch.
##                      3.  Default if above cases not true:
##                          -   Checkout a new local branch at the current SHA.
##  
##  @param[in]      branch              Name of branch to checkout
##  @param[in]      remote              [Optional] Remote to use when checking out the branch. When 
##                                      none provided, uses 'origin'.
##  
##  @retval         0                   Successfully checked out the requested branch.
####################################################################################################
function checkout_branch() {
    
    local branch="${1}"
    local remote="${2:-origin}"
    
    local active="$(get_name_of_current_branch)"
    
    if [[ "${branch}" == "${active}" ]]; then 
        print_message "Branch '${branch}' already active. Skipping."
        return
    fi
    
    print_message "Checking out branch '${branch}'"
    
    if [ $(local_branch_exists "${branch}") -eq 0 ]; then
        git checkout "${branch}"
    elif [ $(remote_branch_exists "${branch}" "${remote}") -eq 0 ]; then
        git checkout -b "${branch}" "${remote}/${branch}"
    else
        git checkout -b "${branch}"
    fi

}


####################################################################################################
##  @fn             checkout_most_recently_active_branch()
##  
##  @brief          Checkout the branch on the target remote with the most recent activity.
##  
##  @details        Uses the git 'branch -r' command along with grep, git 'show', head, sort, and 
##                  awk to get the most recently commit on a target remote. This is then used to 
##                  determine which branch should be checked out.
##                  
##                  @par The actual branch checking out is handled by invoking 'checkout_branch()'.
##  
##  @note           Inspiration for method:
##                      -   https://gist.github.com/jasonrudolph/1810768
##  
##  @param[in]      remote              [Optional] Remote to use. When none provided, uses 'origin'.
## 
##  @retval         0                   Successfully checked out the most recently active branch.
##  @retval         1                   Invalid remote.
####################################################################################################
function checkout_most_recently_active_branch() {
    
    local remote="${1:-origin}"
    
    if ! [ $(remote_exists "${remote}") -eq 0 ]; then
        return 1
    fi
    
    local tmp="$(for branch in `git branch -r | grep -v HEAD | grep "${remote}"`; do echo -e `git show --format='%ci' $branch | head -n 1` \\t$branch; done | sort -r | head -n 1 | awk '{$1=$1}$1')"
    
    local timestamp=$(echo ${tmp} | cut -d'/' -f1)
    local branch=$(echo ${tmp} | cut -d'/' -f2)
    
    print_message "Branch with most recent activity is '${branch}' on '${timestamp}'"
    
    checkout_branch "${branch}"
}


