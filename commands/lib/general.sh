#!/bin/bash
####################################################################################################
##  @file
##  
##  @brief          Library file providing general helper functions.
##  
##  @depends
##                  - BASH      [4.0++]
##                  - git       [2.11.0++]
##  
##  @version        1.0.0
####################################################################################################


#===================================================================================================
# [Initial] Working directory related helper functions
#---------------------------------------------------------------------------------------------------
####################################################################################################
##  @fn             set_working_directory()
##  
##  @brief          Define the provided path as the working directory for the duration of the 
##                  execution of the calling script.
##  
##  @details        Creates a read only variable to track the provided path.
##  
##  @warning        Once successfully set, the value for the '_working_directory' variable cannot be 
##                  changed!
##  
##  @param[in] path                Path to set as working directory.
##  
##  @retval         0                   Successfully set path.
##  @retval         1                   Working directory already defined (may only assign once).
##  @retval         2                   Value provided for path missing or invalid.
#####################################################################################################
function set_working_directory() {
    
    local path="${1}"
    
    if ! [ -z ${_working_directory+x} ]; then
        print_verbose_message "Working directory already defined as '${_working_directory}'. Cannot set to '${path}'"
        return 1
    elif [[ "${path}" != "" ]] && [ -d "${path}" ]; then
        readonly _working_directory="${path}"
        return 0
    fi
    
    print_verbose_message "Provided path '${path}' is not a valid value for a working directory"
    return 2
}


####################################################################################################
##  @fn             navigate_to_working_directory()
##  
##  @brief          Navigates to the current 'working directory' [if defined].
##  
##  @details        Wrapper call to "cd ${_working_directory}.
##  
##  @global         _working_directory  Working directory to navigate to using the cd command.
##  
##  @retval         0                   Successfully changed to working directory.
##  @retval         1                   Working directory not defined or not a directory.
####################################################################################################
function navigate_to_working_directory() {
    
    if ! [ -z ${_working_directory+x} ] && [ -d "${_working_directory}" ]; then
        cd "${_working_directory}"
        return 0
    else
        print_verbose_message "No valid working directory defined (skipping changes to present working directory)"
        return 1
    fi
}


####################################################################################################
##  @fn             exit_script()
##  
##  @brief          Wrapper function for exiting the script with the provided exit code and an 
##                  optional message.
##  
##  @details        Prints optional message and returns to the original working directory before 
##                  exiting with the provided exit code.
##  
##  @note           Non-zero exit codes have their messages [if non-empty] wrapped with the string 
##                  "Exception: ...".
##  
##  @global         _working_directory  Working directory to change to before exiting the script.
##  
##  @param[in]      exit_code           Exit code to use when exiting this script.
##  @param[in]      message             [Optional] String to print to the terminal before exiting.
##  
##  @retval         return_code         Value provided to the function as this desired exit code.
####################################################################################################
function exit_script() {
    
    local exit_code=${1}
    local message="${2}"
    
    navigate_to_working_directory
    
    if [[ "${message}" != "" ]]; then
        if [ ${exit_code} -gt 0 ]; then
            print_message "Exception:  ${message}"
        else
            print_message "${message}"
        fi
    fi
    
    exit $exit_code
}




#===================================================================================================
# Fallback directory related helper functions
#---------------------------------------------------------------------------------------------------
####################################################################################################
##  @fn             set_fallback_directory()
##  
##  @brief          Define the provided path as the fallback directory to track for returning to 
##                  later if desired.
##  
##  @details        Updates a global variable used to track the fallback path. If value is invalid, 
##                  the tracking variable will be cleared.
##  
##  @param[in]      path                Path to set as fallback directory.
##  
##  @retval         0                   Successfully set path.
##  @retval         1                   Value provided for path missing or invalid.
####################################################################################################
function set_fallback_directory() {
    
    local path="${1}"
    
    if [[ "${path}" != "" ]] && [ -d "${path}" ]; then
        _fallback_directory="${path}"
        return 0
    fi
    
    print_verbose_message "Provided path '${path}' is not a valid directory path. Wiping fallback directory."
    _fallback_directory=""
    return 1
}


####################################################################################################
##  @fn             navigate_to_fallback_directory()
##  
##  @brief          Navigates to the current 'fallback directory' [if defined].
##  
##  @details        Wrapper call to "cd ${_fallback_directory}.
##  
##  @global         _fallback_directory Fallback directory to navigate to using the cd command.
##  
##  @retval         0                   Successfully changed to fallback directory.
##  @retval         1                   Fallback directory not defined or not a directory.
####################################################################################################
function navigate_to_fallback_directory() {
    
    if ! [ -z ${_fallback_directory+x} ] && [ -d "${_fallback_directory}" ]; then
        cd "${_fallback_directory}"
        return 0
    else
        print_verbose_message "No valid fallback directory defined (skipping changes to present working directory)"
        return 1
    fi
}


####################################################################################################
##  @fn             return_from_function()
##  
##  @brief          Wrapper function for returning from a parent [caller] function with the provided 
##                  return code and an optional message.
##  
##  @details        Prints optional message and returns to the original working directory before 
##                  returning with the provided return code.
##  
##  @note           Non-zero return codes have their messages [if non-empty] wrapped with the string 
##                  "Exception: ...".
##  
##  @note           The logic in this function is based heavily on the following reference:
##                      -   https://stackoverflow.com/questions/58103370/bash-return-two-function-levels-two-nested-calls
##  
##  @global         _fallback_directory Fallback directory to change to before returning to the 
##                                      caller of the previous function.
##  
##  @param[in]      return_code         Return code to use when returning up two levels.
##  @param[in]      message             [Optional] String to print to the terminal before returning.
##  
##  @retval         return_code         Value provided to the function as this desired return code.
####################################################################################################
function return_from_function() {
    
    local return_code=${1}
    local message="${2}"
    
    navigate_to_fallback_directory
    
    if [[ "${message}" != "" ]]; then
        if [ ${return_code} -gt 0 ]; then
            print_message "Exception:  ${message}"
        else
            print_message "${message}"
        fi
    fi
    
    trap 'trap "shopt -u extdebug; trap - DEBUG; return ${return_code}" DEBUG; return 2' DEBUG
    shopt -s extdebug
}




#===================================================================================================
# Git repository helper functions
#---------------------------------------------------------------------------------------------------
####################################################################################################
##  @fn             ensure_in_repository_root()
##  
##  @brief          Locates the root of the current git repository and switches to that directory.
##  
##  @details        Uses the git's 'rev-parse' command to determine the top level [root] of the git 
##                  repository. The top level is then switched to via the 'cd' command (no checks to 
##                  avoid redundant directory changes are done).
##  
##  @warning        Function presumes the current working directory is a git repository.
####################################################################################################
function ensure_in_repository_root() {
    
    local repository_root="$(git rev-parse --show-toplevel)"
    print_verbose_message "Ensuring operating from root of repository"
    cd "${repository_root}"
}


####################################################################################################
##  @fn             execute_command_on_git_repositories()
##  
##  @brief          Apply the provided command to one or more git repositories starting at the 
##                  provided path (precise behavior controlled via mode flags).
##  
##  @details        Starts searching at the provided 'target_directory' for a git repository. If one 
##                  is found, executes the provided '_command'. Additionally supports recursively 
##                  searching for and executing the provided '_command' based on the following mode 
##                  flags:
##                      -   recursive directory searching
##                      -   include nested git repositories
##  
##                  @par The command string is any and all text following the first parameter (which 
##                  is the target directory). The 'shift' command is utilized to drop the first 
##                  parameter (once it is processed) and capture the remaining n-args as the 
##                  '_command' to execute.
##  
##  @param[in]      target_directory    Directory to start the process of searching for git 
##                                      repositories for which to apply the provided '_command'.
##  @param[in]      _command            Command to execute on any and all git repositories. Defined 
##                                      as all parameters following the first parameter.
##  
##  @retval         0                   Executed function without issues.
##  @retval         1                   Requested target directory not found.
##  @retval         2                   No git repository located at the target directory.
##  @retval         3                   Unknown error encountered executing the provided command.
####################################################################################################
function execute_command_on_git_repositories() {
    
    local target_directory="${1}"
    shift
    local _command="${@}"
    local paths_processed=" "
    
    set_fallback_directory "$(pwd)"
    
    cd $target_directory > /dev/null 2>&1
    if ! [ $? -eq 0 ]; then
        return_from_function 1 "Unable to locate target directory '${target_directory}'"
    fi
    
    print_verbose_message "Starting repository search in '${target_directory}'"
    
    # Refresh path to target directory so it is absolute. Needed for string manipulation in terminal output messages.
    # Note:  This only comes into play when recursively searching for repositories.
    target_directory=$(pwd)
    
    if [ $(current_directory_is_git_repository) -eq 1 ]; then 
        
        # Process target directory as a git repository
        paths_processed+=" $(pwd) "
        
        print_message "Invoking provided command in starting directory"
        
        (${_command})
        
        if ! [ $? -eq 0 ]; then
            return_from_function 3 "Failed to execute the provided command for the current git repository"
        fi
        
        if [ $(recursive_mode_is_enabled) -eq 1 ]; then 
            print_verbose_message "Recursive flag detected, starting search for additional repositories"
        else
            return_from_function 0
        fi
    else
        
        # Target directory is not a git repository - bail unless recursive flag set
        
        if [ $(recursive_mode_is_enabled) -eq 0 ]; then 
            return_from_function 2 "Target directory is not a git repository and recursive flag not set"
        else
            print_verbose_message "Target directory is not a git repository but starting recursive search"
        fi
    fi
    
    # Process any and all git repositories found within the node of th starting directory
    find "$(pwd)" -type d -not -path "$(pwd)" -not -path '*/\.git*' -print0 | while IFS= read -r -d '' subdirectory; do 
        
        cd "${subdirectory}"
        
        # Continue processing if the current directory:
        #   1. Is the root of a git repository
        #   2. Has not already been processed
        if [ $(current_directory_is_git_repository) -eq 1 ] && [ $(current_directory_is_repository_root) -eq 1 ]; then
            
            flag_already_processed=0
            for processed in ${paths_processed}; do
                if [[ "$(pwd)" == "${processed}"* ]]; then
                    flag_already_processed=1
                    break
                fi
            done
            
            if [ ${flag_already_processed} -eq 1 ]; then
                if [ $(include_nested_mode_is_enabled) -eq 0 ]; then
                    continue
                else
                    print_verbose_message "Overriding skip to include nested git repository"
                fi
            fi
            
            paths_processed+=" $(pwd) "
            
            # We can use '#' instead of '/' for the sed command to allow removing a path from a path (as paths contain '/').
            print_message "Invoking provided command in subdirectory '$(pwd | sed -e "s#${target_directory}/##g")'"
            
            (${_command})
            
            if ! [ $? -eq 0 ]; then
                return_from_function 3 "Failed to execute the provided command for the current git repository"
            fi
            
        fi
    done
    
    # There is an unknown logic error where invoking the 'return_from_function' at the end of a 
    # script results in an error being raised. Since we only want to ensure we are in the original 
    # directory that was active when this function was invoked, we can call the change directory 
    # wrapper function instead.
    navigate_to_fallback_directory
    return 0
}


