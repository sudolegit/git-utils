#!/bin/bash
####################################################################################################
##  @file
##  
##  @brief          Library file providing helper functions that provide checks that can be used to 
##                  control the flow of a command (e.g. "is this directory a git repository").
##  
##  @depends
##                  - BASH      [4.0++]
##                  - git       [2.11.0++]
##  
##  @version        1.0.0
####################################################################################################


####################################################################################################
##  @fn             current_directory_is_git_repository()
##  
##  @brief          Returns a boolean value indicating if the directory is a git repository or not.
##  
##  @details        Uses the git's 'rev-parse' command to check if the working path is within a 
##                  working tree. All output from the command is suppressed and the return code is 
##                  processed for determining the boolean flag to return.
##  
##  @note           Values are returned via stdout (not via return codes)
##  
##  @retval         0                   Current directory is not a git repository.
##  @retval         1                   Current directory is a git repository.
####################################################################################################
function current_directory_is_git_repository() {
    
    # The following command will print an error if not inside a Git repository and 'true' if inside one.
    # For scripting, it is more effective to shuffle all output to /dev/null and check the return code 
    # (0 == in a git repository).
    #  REF:  https://stackoverflow.com/questions/12641469/list-submodules-in-a-git-repository
    git rev-parse --is-inside-work-tree > /dev/null 2>&1
    echo $(( $? == 0 ? 1 : 0 ))
}


####################################################################################################
##  @fn             current_directory_is_repository_root()
##  
##  @brief          Returns a boolean value indicating if the current directory is the root of the 
##                  current git repository.
##  
##  @details        Uses the git's 'rev-parse' command to determine the top level [root] of the git 
##                  repository. The top level is compared against the working directory to determine 
##                  the boolean flag to return.
##  
##  @warning        Function presumes the current working directory is a git repository.
##  
##  @note           Values are returned via stdout (not via return codes)
##  
##  @retval         0                   Current directory is not the root of the git repository.
##  @retval         1                   Current directory is the root of the git repository.
####################################################################################################
function current_directory_is_repository_root() {
    
    local repository_root=$(git rev-parse --show-toplevel)
    if [[ "$(pwd)" == "${repository_root}" ]]; then
        echo 1
    else
        echo 0
    fi
}


