#!/bin/bash
####################################################################################################
##  @file
##  
##  @brief          Library file for setting and checking the status of runtime flags.
##  
##  @details        Currently supported flags:
##                      -   clean state
##                      -   dry run
##                      -   include nested (recursive)
##                      -   override (force)
##                      -   recursive
##                      -   verbose
## 
##  @warning        Presently the flags in this file can only set set (never cleared). They are 
##                  intentionally "set and forget" runtime flags (vs passing around from function to 
##                  function).
##  
##  @depends
##                  - BASH      [4.0++]
##  
##  @version        1.0.0
####################################################################################################


#===================================================================================================
# Mode:  Clean state
#---------------------------------------------------------------------------------------------------
####################################################################################################
##  @fn             enable_clean_state_mode()
##  
##  @brief          Enables "clean state" support for the current script execution.
##  
##  @details        Creates a read only global variable named '_flag_clean_state' to indicate that 
##                  the repository should be left in a clean state after the current execution of 
##                  this script.
##  
##  @warning        Once set, "clean state" mode cannot be unset.
####################################################################################################
function enable_clean_state_mode() {
    
    print_verbose_message "Enabling \"clean state\" mode"
    
    if [ -z ${_flag_clean_state+x} ]; then
        readonly _flag_clean_state=1
    fi
}


####################################################################################################
##  @fn             clean_state_mode_is_enabled()
##  
##  @brief          Returns a boolean flag indicating if "clean state" support is enabled.
##  
##  @note           Values are returned via stdout (not via return codes)
##  
##  @retval         0                   Support for the "clean state" mode is disabled.
##  @retval         1                   Support for the "clean state" mode is enabled.
####################################################################################################
function clean_state_mode_is_enabled() {
    # Mode is enabled so long as the global variable is defined.
    [ ! -z ${_flag_clean_state+x} ] && echo 1 || echo 0
}




#===================================================================================================
# Mode:  Dry run execution
#---------------------------------------------------------------------------------------------------
####################################################################################################
##  @fn             enable_dry_run_mode()
##  
##  @brief          Enables "dry run" support for the current script execution.
##  
##  @details        Creates a read only global variable named '_flag_dry_run' to indicate that 
##                  permanent changes should be skipped during the current execution of this script.
##  
##  @warning        Once set, "dry run" mode cannot be unset.
####################################################################################################
function enable_dry_run_mode() {
    
    print_verbose_message "Enabling \"dry run\" mode"
    
    if [ -z ${_flag_dry_run+x} ]; then
        readonly _flag_dry_run=1
    fi
}


####################################################################################################
##  @fn             dry_run_mode_is_enabled()
##  
##  @brief          Returns a boolean flag indicating if "dry run" support is enabled.
##  
##  @note           Values are returned via stdout (not via return codes)
##  
##  @retval         0                   Support for the "dry run" mode is disabled.
##  @retval         1                   Support for the "dry run" mode is enabled.
####################################################################################################
function dry_run_mode_is_enabled() {
    # Mode is enabled so long as the global variable is defined.
    [ ! -z ${_flag_dry_run+x} ] && echo 1 || echo 0
}




#===================================================================================================
# Mode:  Include nested git repositories
#---------------------------------------------------------------------------------------------------
####################################################################################################
##  @fn             enable_include_nested_mode()
##  
##  @brief          Enables "include nested" git repository support for the current script 
##                  execution.
##  
##  @details        Creates a read only global variable named '_flag_include_nested' to indicate 
##                  that nested git repositories [if relevant/found] should be processed.
##  
##  @warning        Once set, "include nested" mode cannot be unset.
####################################################################################################
function enable_include_nested_mode() {
    
    print_verbose_message "Enabling \"include nested git repositories\" mode"
    
    if [ -z ${_flag_include_nested+x} ]; then
        readonly _flag_include_nested=1
    fi
}


####################################################################################################
##  @fn             include_nested_mode_is_enabled()
##  
##  @brief          Returns a boolean flag indicating if "include nested" git repository support is 
##                  enabled.
##  
##  @note           Values are returned via stdout (not via return codes)
##  
##  @retval         0                   Support for the "include nested" mode is disabled.
##  @retval         1                   Support for the "include nested" mode is enabled.
####################################################################################################
function include_nested_mode_is_enabled() {
    # Mode is enabled so long as the global variable is defined.
    [ ! -z ${_flag_include_nested+x} ] && echo 1 || echo 0
}




#===================================================================================================
# Mode:  Override current state (force)
#---------------------------------------------------------------------------------------------------
####################################################################################################
##  @fn             enable_override_mode()
##  
##  @brief          Enables "override" support for the current script execution.
##  
##  @details        Creates a read only global variable named '_flag_override' to indicate that 
##                  overriding actions should be taken during the current execution of this script.
##  
##  @warning        Once set, "override" mode cannot be unset.
####################################################################################################
function enable_override_mode() {
    
    print_verbose_message "Enabling \"override\" mode"
    
    if [ -z ${_flag_override+x} ]; then
        readonly _flag_override=1
    fi
}


####################################################################################################
##  @fn             override_mode_is_enabled()
##  
##  @brief          Returns a boolean flag indicating if "override" support is enabled.
##  
##  @note           Values are returned via stdout (not via return codes)
##  
##  @retval         0                   Support for the "override" mode is disabled.
##  @retval         1                   Support for the "override" mode is enabled.
####################################################################################################
function override_mode_is_enabled() {
    # Mode is enabled so long as the global variable is defined.
    [ ! -z ${_flag_override+x} ] && echo 1 || echo 0
}




#===================================================================================================
# Mode:  Include subdirectories (recursive searches)
#---------------------------------------------------------------------------------------------------
####################################################################################################
##  @fn             enable_recursive_mode()
##  
##  @brief          Enables recursive mode for processing directories.
##  
##  @details        Creates a read only global variable named '_flag_recursive_mode' to indicate that 
##                  recursive mode is desired for processing directories.
##  
##  @warning        Once set, recursive mode cannot be unset.
####################################################################################################
function enable_recursive_mode() {
    
    print_verbose_message "Enabling \"recursive directory search\" mode"
    
    if [ -z ${_flag_recursive_mode+x} ]; then
        readonly _flag_recursive_mode=1
    fi
}


####################################################################################################
##  @fn             recursive_mode_is_enabled()
##  
##  @brief          Returns a boolean flag indicating if "recursive" support is enabled.
##  
##  @note           Values are returned via stdout (not via return codes)
##  
##  @retval         0                   Support for the "recursive" mode is disabled.
##  @retval         1                   Support for the "recursive" mode is enabled.
####################################################################################################
function recursive_mode_is_enabled() {
    # Mode is enabled so long as the global variable is defined.
    [ ! -z ${_flag_recursive_mode+x} ] && echo 1 || echo 0
}




#===================================================================================================
# Mode:  Verbose terminal output
#---------------------------------------------------------------------------------------------------
####################################################################################################
##  @fn             enable_verbose_mode()
##  
##  @brief          Enables verbose mode for terminal output.
##  
##  @details        Creates a read only global variable named '_flag_verbose_mode' to indicate that 
##                  verbose mode is desired for terminal output.
##  
##  @warning        Once set, verbose mode cannot be unset.
####################################################################################################
function enable_verbose_mode() {
    
    if [ -z ${_flag_verbose_mode+x} ]; then
        readonly _flag_verbose_mode=1
    fi
    
    print_verbose_message "Enabling \"verbose terminal output\" mode"
}


####################################################################################################
##  @fn             verbose_mode_is_enabled()
##  
##  @brief          Returns a boolean flag indicating if "verbose" support is enabled.
##  
##  @note           Values are returned via stdout (not via return codes)
##  
##  @retval         0                   Support for the "verbose" mode is disabled.
##  @retval         1                   Support for the "verbose" mode is enabled.
####################################################################################################
function verbose_mode_is_enabled() {
    # Mode is enabled so long as the global variable is defined.
    [ ! -z ${_flag_verbose_mode+x} ] && echo 1 || echo 0
}


