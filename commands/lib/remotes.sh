#!/bin/bash
####################################################################################################
##  @file
##  
##  @brief          Library file providing helper functions related to interacting with git remote 
##                  settings.
##  
##  @depends
##                  - BASH      [4.0++]
##                  - git       [2.11.0++]
##  
##  @version        1.0.0
####################################################################################################


####################################################################################################
##  @fn             __translate_remote_url()
##  
##  @brief          Translates the provided remotes URL to the provided protocol.
##  
##  @details        Strips any known prefixes (e.g. 'git@', 'http://', ...) from the provided 'url', 
##                  breaks out any username detected in the 'url' string, and formats a new URL to 
##                  match the desired 'protocol'.
##  
##  @warning        Presumes the 'url' string provided is non-empty, should be translated, and that 
##                  the provided 'protocol' is a valid type (if an invalid protocol, silently 
##                  returns the original string).
##  
##  @warning        This is meant to be an internal / private function. As such, it does not do as 
##                  much validation as other functions may do.
##  
##  @param[in]      url                 URL string to tranlsate.
##  @param[in]      protocol            Protocol to use when tranlasting the provided 'url'. Values 
##                                      should be a lowercase string matching a value from the 
##                                      following set:  {http, https, ssh}.
####################################################################################################
function __translate_remote_url() {
    
    local url="${1}"
    local protocol="${2}"
    
    # Strip off any 'git@', 'http://', 'https://' prefix from the provided URL
    local pruned="$(echo "${url}" | sed -e "s/http:\/\///g" -e "s/https:\/\///g" -e "s/git@//g")"
    
    # Sanitize string in case it is a SSH protocol string (change ':' => '/')
    local sanitized="$(echo "${pruned}" | sed -e "s/:/\//g")"
    
    # Break out username from base_url if it exists. If found, add '@/' suffix to the string so the 
    # next section that builds the string has less processing to do (include the suffix dividing the 
    # username from the base_url as one string).
    if [[ "${sanitized}" == *"@"* ]]; then
        local username="${sanitized%@*}@/"
        local base_url="${sanitized#*@}"
    else
        local username=""
        local base_url="${sanitized}"
    fi
    
    # Structure a new URL in the format of the desired protocol
    if [[ "${protocol}" == "http" ]]; then 
        echo "http://${username}${base_url}"
    elif [[ "${protocol}" == "https" ]]; then 
        echo "https://${username}${base_url}"
    elif [[ "${protocol}" == "ssh" ]]; then 
        echo "git@${base_url}" | sed -e "s/\//:/"
    else
        # Error prevention:  Silently return the original string so no null string manipulation by 
        # the caller.
        echo "${url}"
    fi
    
}


####################################################################################################
##  @fn             remote_exists()
##  
##  @brief          Determine if the remote name provided exists in the present list of remotes.
##  
##  @details        Uses the git 'remote', grep, and wc to determine if the remote name provided 
##                  exists in the present list of a remotes.
##  
##  @param[in]      remote              Name of remote to check for in repository.
##  
##  @retval         0                   Remote exists.
##  @retval         1                   Missing required parameter(s) to function.
##  @retval         2                   Remote does not exist.
####################################################################################################
function remote_exists() {
    
    local remote="${1:-NULL}"
    
    if [[ "${remote}" == "NULL" ]]; then
        echo 1
        return
    fi
    
    if [ $(git remote | grep ^"${remote}"$ | wc -l) -gt 0 ]; then
        echo 0
    else
        echo 2
    fi
}


####################################################################################################
##  @fn             remote_accessible()
##  
##  @brief          Determine if the remote provided is currently accessible.
##  
##  @details        Confirms the remote exists before then using the git 'remote get-url' command to
##                  confirm the URL is valid.
##  
##  @note           Uses the ping command to confirm if the network can access 'google.com' as a 
##                  mitigation of command timeout issues with 'git ls-remote'.
##  
##  @warning        This function will take a long time to process on failed commands (i.e. missing 
##                  remotes) unless there are external throttle limits in place (upwards of 10++ 
##                  seconds).
##  
##  @param[in]      remote              Name of remote to check.
##  
##  @retval         0                   Remote exists.
##  @retval         1                   Missing required parameter(s) to function.
##  @retval         2                   Remote does not exist.
##  @retval         3                   Unable to access internet.
##  @retval         4                   Remote not accessible.
####################################################################################################
function remote_accessible() {
    
    local remote="${1:-NULL}"
    
    if [[ "${remote}" == "NULL" ]]; then
        echo 1
        return
    fi
    
    if ! [ $(remote_exists "${remote}") -eq 0 ]; then
        echo 2
    else
        
        ping google.com -c 1 -W 1 > /dev/null 2>&1
        
        if [ $? -eq 0 ]; then
            
            git ls-remote $(git remote get-url "${remote}") > /dev/null 2>&1
            
            if [ $? -eq 0 ]; then
                echo 0
                return
            else
                echo 4
            fi
        
        else
            echo 3
        fi
    fi
}


####################################################################################################
##  @fn             set_remote_protocol()
##  
##  @brief          Sets the protocol for a single remote in the current git repository.
##  
##  @details        Runs through the following logic flow to set the protocol for the desired 
##                  remote:
##                      -   Validates input parameters.
##                      -   Determines the present protocol usd for the provided remote.
##  
##  @warning        Function presumes the current working directory is a git repository.
##  
##  @param[in]      protocol            Which protocol to utilize for the remote. Values should be 
##                                      a lowercase string matching a value from the following set:  
##                                      {http, https, ssh}.
##  @param[in]      remote              Name of remote to modify.
##  
##  @retval         0                   Function exiting without issues.
##  @retval         1                   Invalid 'protocol' requested.
##  @retval         2                   Invalid 'remote' requested (not found in target repository).
##  @retval         3                   Current protocol for the requested remote is not understood 
##                                      / not able to be converted.
##  @retval         4                   Remote already using the requested protocol.
####################################################################################################
function set_remote_protocol() {
    
    local protocol="${1}"
    local remote="${2:-NULL}"
    
    # Validate function parameters
    if [[ "${protocol}" != "http" ]] && [[ "${protocol}" != "https" ]] && [[ "${protocol}" != "ssh" ]] ; then
        print_verbose_message "Protocol '${protocol}' is unsupported (must be http, https, or ssh)"
        return 1
    elif ! [ $(git remote | grep -x "${remote}" | wc -l) -eq 1 ]; then
        print_verbose_message "Remote requested is not in the current git repository"
        return 2
    fi
    
    # Use the structure of the remote's URL to infer the protocol
    local remote_url="$(git remote get-url "${remote}")"
    local remote_protocol=""
    if [[ "${remote_url}" == "http://"* ]]; then 
        remote_protocol="http"
    elif [[ "${remote_url}" == "https://"* ]]; then 
        remote_protocol="https"
    elif [[ "${remote_url}" == "git@"* ]]; then 
        remote_protocol="ssh"
    fi
    
    # Validate the protocol provided and see if we should change it
    if  [[ "${remote_protocol}" != "" ]]; then
        if [ "${protocol}" != "${remote_protocol}" ]; then
            
            print_verbose_message "Will convert protocol from '${remote_protocol}' to '${protocol}'"
            
            new_url="$(__translate_remote_url "${remote_url}" "${protocol}")"
            
            print_message "Changing remote url from '${remote_url}' to '${new_url}'"
            
            if [ $(dry_run_mode_is_enabled) -eq 0 ]; then
                git remote set-url "${remote}" "${new_url}"
            else
                print_verbose_message "Dry run flag detected, skipping final command to change URL."
            fi
            
        else
            print_verbose_message "Remote already uses desired protocol '${protocol}'. Skipping."
            return 4
        fi
    else
        print_verbose_message "Remote currently uses an unsupported protocol (likely a local path). Skipping."
        return 3
    fi
}


####################################################################################################
##  @fn             execute_command_on_git_repository_remotes()
##  
##  @brief          Apply the provided command to one or more git remotes.
##  
##  @details        Runs through the following logic flow to set the protocol for the desired 
##                  remotes:
##                      -   Ensures working from the root of the repository,
##                          -   Required so able to parse the git submodules file.
##                      -   Gather a list of all directories to check (root as well as any 
##                          initialized submodules).
##                      -   Loop over the array of determined directories and:
##                          -   Loop over the desired remote(s) and invoke the provided '_command' 
##                              with each 'remote' as the final parameter provided to the command.
##                      -   Ensure execution exits function in the same directory as when invoked.
##  
##  @warning        Function presumes the current working directory is a git repository.
##  
##  @warning        The command provided will have one additional parameter provided:  the 'remote' 
##                  name for the instance of running the command. This will never be 'all' and will 
##                  always be appended to the tail end of the invoked command.
##  
##  @warning        No checks are done against the return code from invoking the '_command' 
##                  (processing will continue even if an error is encountered).
##  
##  @param[in]      target_remote       Name of remote(s) to process (should be 'all' or the name of
##                                      a specific remote).
##  @param[in]      _command            Command to execute on any and all git repository remotes. 
##                                      Defined as all parameters following the first parameter.
##  
##  @retval         0                   Function exiting without issues.
##  @retval         1                   Failed to find a remote matching the provided name.
####################################################################################################
function execute_command_on_git_repository_remotes() {
    
    local target_remote="${1}"
    shift
    local _command="$@"
    
    set_fallback_directory "$(pwd)"
    
    ensure_in_repository_root
    
    # Locate all paths to process (current directory + any submodules)
    print_verbose_message "Searching for git submodules"
    local _IFS="${IFS}"
    IFS="|"
    local paths=($(pwd))
    for submodule in $(git config --file .gitmodules --get-regexp path | cut -d' ' -f2); do
        if [ -e ${submodule}/.git ]; then
            print_verbose_message "Including submodule '${submodule}' in processing"
            paths+="("$(pwd)/$(echo ${submodule} )")"
        else
            print_verbose_message "Skipping submodule '${submodule}' (not initialized)"
        fi
    done
    IFS="${_IFS}"
    
    # Loop over all repositories [paths] and execute the provided command on each [relevant] remote
    for path in "${paths[@]}" ; do 
        
        cd "${path}"
        print_message "Checking for remote(s) in repository '$(basename "$(pwd)")'"
        
        flag_found_target_remote=0
        for remote in $(git remote); do
            
            if [[ "${target_remote}" == "all" ]] || [[ "${target_remote}" == "${remote}" ]]; then
                
                flag_found_target_remote=1
                print_verbose_message "Running provided command against remote '${remote}'"
                
                #===================================================================================
                # This is where the command provided is invoked to be run against a relevant remote 
                # on the present git repository. Always pass the current 'remote' as the last 
                # parameter to the called function
                #-----------------------------------------------------------------------------------
                ${_command} "${remote}"
                #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                
            else
                print_verbose_message "Remote '${remote}' not in list of remotes. Skipping."
            fi
            
        done
        
        if [ ${flag_found_target_remote} -eq 0 ]; then
            return_from_function 1 "Failed to find a remote matching the target remote named '${target_remote}'"
        fi
    done
    
    # There is an unknown logic error where invoking the 'return_from_function' at the end of a 
    # script results in an error being raised. Since we only want to ensure we are in the original 
    # directory that was active when this function was invoked, we can call the change directory 
    # wrapper function instead.
    navigate_to_fallback_directory
    return 0
    
}


