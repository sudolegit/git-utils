#!/bin/bash
####################################################################################################
##  @file
##  
##  @brief          Library file providing helper functions that provide queries into the state of a 
##                  repository.
##  
##  @depends
##                  - BASH      [4.0++]
##                  - git       [2.11.0++]
##  
##  @version        1.0.0
####################################################################################################


####################################################################################################
##  @fn             number_of_modified_files()
##  
##  @brief          Reports back the number of files in the repository which are tracked and have 
##                  been modified.
##  
##  @details        Uses the 'git diff' command to list the names of all modified files and pipes 
##                  the output into the word count [wc] command to determine the number of modified 
##                  files.
##  
##  @warning        The number reported does not include any staged changes.
##  
##  @retval         count               Count of the number of modified files in the repository.
####################################################################################################
function number_of_modified_files() {
    echo $(git diff --name-only | wc -l)
}


####################################################################################################
##  @fn             number_of_staged_files()
##  
##  @brief          Reports back the number of files in the repository which are staged and ready 
##                  to be committed.
##  
##  @details        Uses the 'git diff' command with the '--cached' flag to list the names of all 
##                  staged files and pipes the output into the word count [wc] command to determine 
##                  the number of staged files.
##  
##  @retval         count               Count of the number of staged files in the repository.
####################################################################################################
function number_of_staged_files() {
    echo $(git diff --name-only --cached | wc -l)
}


####################################################################################################
##  @fn             number_of_untracked_files()
##  
##  @brief          Reports back the number of untracked files in the repository.
##  
##  @details        Uses the 'git status' command to list a short summary of all untracked files. 
##                  This list will include modified files. To filter out the modified files, the 
##                  output from the 'git status' command is piped into grep to specifically filter 
##                  for lines starting with '?? '. The pruned list is then counted via the word count 
##                  [wc] command.
##  
##  @warning        The number reported does not include any staged changes.
##  
##  @retval         count               Count of the number of untracked files in the repository.
####################################################################################################
function number_of_untracked_files() {
    echo $(git status --short --untracked-files=all | grep "^?? " | wc -l)
}


####################################################################################################
##  @fn             repository_state_is_clean()
##  
##  @brief          Returns a boolean flag indicating if the repository is in a clean state (no 
##                  modified, staged, or untracked files).
##  
##  @details        Uses the 'number_of_...()' functions to check the state of the repository. If 
##                  the count is zero for all checks, returns true.
##  
##  @retval         0                   Repository is not clean.
##  @retval         1                   Repository is clean.
####################################################################################################
function repository_state_is_clean() {
    
    if  [ $(number_of_modified_files)   -eq 0 ] &&  \
        [ $(number_of_staged_files)     -eq 0 ] &&  \
        [ $(number_of_untracked_files)  -eq 0 ]
    then 
        echo 1
    else
        echo 0
    fi
}


####################################################################################################
##  @fn             repository_state_is_clean_enough()
##  
##  @brief          Returns a boolean flag indicating if the repository is in a "clean enough" state 
##                  (no modified or staged files).
##  
##  @details        Uses the 'number_of_...()' functions to check the state of the repository. If 
##                  the count is zero for all checks, returns true.
##  
##  @note           Function created to allow checking if changing branches is possible. 
##                  Essentially, so long as there are no tracked or staged changes we can change 
##                  branches safely. No checks on untracked files are made.
##  
##  @retval         0                   Repository is not clean enough.
##  @retval         1                   Repository is clean enough.
####################################################################################################
function repository_state_is_clean_enough() {
    
    if  [ $(number_of_modified_files)   -eq 0 ] &&  \
        [ $(number_of_staged_files)     -eq 0 ]
    then 
        echo 1
    else
        echo 0
    fi
}


####################################################################################################
##  @fn             repository_head_is_detached()
##  
##  @brief          Returns a boolean flag indicating if the repository is in a detached (headless) 
##                  state.
##  
##  @details        Uses the 'git branch' command to list the current branch. If no value is 
##                  returned, a detached (headless) state is presumed.
##  
##  @retval         0                   Repository has an active local branch.
##  @retval         1                   Repository is in a detached (headless) state.
####################################################################################################
function repository_head_is_detached() {
    [ $(git branch --show-current | wc -l) -eq 0 ] && echo 1 || echo 0
}


