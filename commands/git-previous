#!/bin/bash
####################################################################################################
##  @file
##  
##  @brief          Moves the current branch pointer to the previous commit on the active branch 
##                  path.
##  
##  @details        Determines the SHA of the current checkout point on the repository and its 
##                  position overall within the local branch history. If there is an accessible 
##                  (local) commit on the current branch immediately preceding the current checkout 
##                  point, checks out to that point.
##
##  @note           Changing commits will be aborted if the repository has any changes to tracked 
##                  files unless the '-f|--force' flag is provided.
##  
##  @param[in]      a|all-branches          [Optional] When provided, all branches are searched when 
##                                          locating the previous viable commit to walk to.
##  @param[in]      c|clean                 [Optional] When provided, the repository will be left in 
##                                          a clean state (no untracked changes, modified files, 
##                                          etc.)
##  @param[in]      f|force                 [Optional] When provided, the switch to the previous 
##                                          commit will be forced (removes any tracked changes).
##  @param[in]      h                       [Optional] When provided, displays help message and 
##                                          exits.
##  @param[in]      v|verbose               [Optional] When provided, the output to the terminal 
##                                          will include additional messages.
##  
##  @retval         0                       Executed script without issues.
##  @retval         1                       Command format is invalid.
##  @retval         2                       Repository is in a 'detached HEAD' state.
##  @retval         3                       Repository has uncommitted changes (can override via the 
##                                          force flag).
##  @retval         4                       No commit available to switch to (at start of local 
##                                          branch path).
##  @retval         5                       Failed to checkout to the previous point on the active 
##                                          branch.
##  
##  @version        - 1.0.0
####################################################################################################


#===================================================================================================
# Define script globals
#---------------------------------------------------------------------------------------------------
flag_search_all_branches=0                                                      # Flag indicating that all branches should be searched.


#===================================================================================================
# @note             Determine root location [directory] for the repository installation.
#---------------------------------------------------------------------------------------------------
# Resolve '$PATH_TO_FILE' until the file is no longer a symlink.
PATH_TO_FILE="${BASH_SOURCE[0]}"
while [ -h "${PATH_TO_FILE}" ]; do
	# Grab next component of PATH_TO_FILE 
	SCRIPT_DIRECTORY="$( cd -P "$( dirname "${PATH_TO_FILE}" )" && pwd )"
	PATH_TO_FILE="$(readlink "${PATH_TO_FILE}")"
	
	# If $PATH_TO_FILE was a relative symlink, we need to resolve it relative to the path where the 
    # symlink file was located.
	[[ ${PATH_TO_FILE} != /* ]] && PATH_TO_FILE="${SCRIPT_DIRECTORY}/${PATH_TO_FILE}"
done

# Define base script directory and root directory for installation.
SCRIPT_DIRECTORY="$( cd -P "$( dirname "${PATH_TO_FILE}" )" && pwd )"

# Load in library functions available to all BASH utility scripts.
source ${SCRIPT_DIRECTORY}/lib/source-all.sh

# Set working directory so calls to 'exit_script' will return the shell to the current directory 
# before exiting the script.
set_working_directory "$(pwd)"


#===================================================================================================
# Process command line arguments
#---------------------------------------------------------------------------------------------------
while [[ ! -z $1  ]]; do
    case $1 in
        # Note:  '--help' is stolen by git itself and '-r' is reserved for commands which use '--recursive'.
        -h)
            print_message ""
            print_message "Usage: git $(basename $0 | cut -d'-' -f2-) [OPTIONS]"
            print_message ""
            print_message "Move branch pointer to the previous commit."
            print_message ""
            print_message "Warnings and notes:"
            print_message ""
            print_message "    1.  This command walks only the active branch by default."
            print_message ""
            print_message "    2.  This command includes both local and remote branches (e.g. refs/heads/[branch] and "
            print_message "        refs/remotes/[remote]/[branch])."
            print_message ""
            print_message "    3.  The command will abort if any local (tracked or staged) changes are found."
            print_message "        -   To override this behavior, please provide the force flag."
            print_message ""
            print_message "The following [OPTIONAL] features are also supported:"
            print_message ""
            print_message "  -a, --all-branches         include all branches when walking the git history"
            print_message "  -c, --clean-state          ensure a clean state (delete untracked changes)"
            print_message "  -f, --force                force the action (discard local changes)"
            print_message "  -v, --verbose              print additional debug messages to the terminal (over stdout)"
            print_message "  -h, --help                 display this help and exit"
            print_message ""
            exit 0
            ;;
        -a|--all-branches)
            flag_search_all_branches=1
            ;;
        -c|--clean-state)
            enable_clean_state_mode
            ;;
        -f|--force)
            enable_override_mode
            ;;
        -v|--verbose)
            enable_verbose_mode
            ;;
        *)
            exit_script 1 "Unknown parameter '$1'"
            ;;
    esac
shift
done


#===================================================================================================
# Main processing loop
#---------------------------------------------------------------------------------------------------
# Ensure we are checked out to a local branch
if [ $(repository_head_is_detached) -eq 1 ]; then
    exit_script 2 "Aborting due to repository being in a 'detached HEAD' state"
else
    print_message "Looking for previous commit on branch '$(get_name_of_current_branch)'"
fi

# Ensure we are starting from a relatively clean state (untracked files are fine).
if [ $(repository_state_is_clean_enough) -eq 0 ]; then
    if [ $(override_mode_is_enabled) -eq 0 ]; then
        exit_script 3 "Aborting due to uncommitted changes (override with force flag)"
    else
        print_message "Uncommitted changes detected. Resetting to a clean state."
        command_output="$(git reset --hard)"
        print_verbose_message "${command_output}"
    fi
fi

# Execute switch to the previous available commit on the current branch
walk_branch down 1 ${flag_search_all_branches}
return_code=$?

if [ ${return_code} -eq 1 ]; then
    exit_script 4 "At start of branch (no commits available to switch to)"
elif [ ${return_code} -gt 0 ]; then 
    exit_script 5 "Failed to switch to previous point on branch (walk_branch returned '${return_code}')"
fi

# Force a clean state if flag set and we've reached this point
if [ $(number_of_untracked_files) -gt 0 ]; then

    if [ $(clean_state_mode_is_enabled) -eq 1 ]; then
        print_message "Cleaning repository to remove all untracked changes"
        command_output="$(git clean -xfd)"
        print_verbose_message "${command_output}"
    else
        print_message "Ignoring untracked changes"
    fi
else
    print_message "Repository is clean (no untracked changes)"
fi

print_message "Successfully changed to the previous commit on the active branch"
exit_script 0


