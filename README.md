# Background

The 'Git Utils' utility aims to aggregate git related aliases and commands used to customize my 
typical workflow.

Feel free to suggest any suggestions, comments, or bugs to the core development project and I will
do my best to integrate them. Bare in mind that development is focused on improving my experience 
as a developer with repetitive tasks and is likely not to become a final [polished] product. 


<br/><br/>

----------------------------------------------------------------------------------------------------
## Project Components

The project is broken down into a few sub-directories:

1. <aliases>
    * Git aliases that can be imported into a git alias file

2. <commands>
    * Custom scripts which expand the default list of available git commands.

3. <docs>
    * Provides all documentation on the project.


<br/><br/>

----------------------------------------------------------------------------------------------------
## Installation [Linux]

1. Clone this repository to your desired location (e.g. ~/.git-utils).

2. Add the <commands> directory to the system PATH.
    
    * For example, you can add a line such as the following to your ~/.bashrc file:
        
        * PATH="${HOME}/.git-utils/commands:${PATH}"

3. Hook in the main alias file (which itself may include additional alias files) in your global 
   gitconfig file (i.e. ~/.gitconfig). For example:
    
        # Pull in external alias definitions from the 'git-utils' repository.
        [include]
          path = ~/.git-utils/aliases/common
    
    * Key things to keep in mind:
        
        * The above command should be standalone (not inside a section within the gitconfig file).
        
        * The path depends on where you choose to install the utility.


<br/><br/>

----------------------------------------------------------------------------------------------------
## Dependencies

The utility presently requires the following programs with at least the version listed:

   * BASH (v4.4.12++)
   * git (v2.11.0++)


<br/><br/>

----------------------------------------------------------------------------------------------------
## Detailed Overview

For complete details on how to use, modify, and expand this utility, please see the provided 
[Doxygen Summary](docs/Overview.html)


<br/><br/>

----------------------------------------------------------------------------------------------------
## Development History

For complete details on the what was changed for the latest release, please see
the [Changelog.md](Changelog.md)


<br/><br/>

----------------------------------------------------------------------------------------------------
## Licensing

All code is provided 'as is'. You are free to modify, distribute, etc. the code within the bounds
of the [Mozilla Public License (v2.0)](LICENSE.txt).




