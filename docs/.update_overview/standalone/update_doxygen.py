####################################################################################################
# @file
# 
# @brief            Updates Doxygen summary [HTML and LaTeX output] for a provided Doxyfile.
# 
# @details          Locates the provided Doxyfile and processes it using the Doxygen utility. 
# 
# @warning          If no Doxyfile is provided, the script will search upwards for a file named 
#                   'Doxyfile.conf' starting at the location of this script. A max of '3' levels 
#                   will be checked and the first instance found will be used. If no instance is 
#                   found, the script will exit with an error.
# 
# @param[in]        doxyfile                Path to Doxyfile to process.
# @param[in]        flags                   Any of the supported flags:
#                                               - -s | --silent
# 
# @attention        DEPENDENCIES:
#                       - Python {2.7.9++; 3.4.2++}
#                       - Doxygen 1.8.8++
# 
# @retval           0                       Successful completion of script.
# @retval           1                       Error code indicating unable to locate a valid Doxyfile.
# @retval           2                       Error code indicating unable to invoke the Doxygen 
#                                           utility.
####################################################################################################


####################################################################################################
# IMPORT MODULES
####################################################################################################
import sys, os                                                                  # General system level access/commands.
import argparse                                                                 # Adds support for processing command line arguments.
import subprocess                                                               # Used to invoke system commands.

# Ensure 'press any key' will work for Python 3 instances.
if sys.version[0] >= "3":
    raw_input=input




####################################################################################################
# DEFINE COMMAND LINE ARGUMENTS
####################################################################################################
# Create parser instance with description.
# >> NOTE:  The optional 'allow_abbrev=False' to exclude partial matches is not available in the 
#           present version of Python 2.7.X accessible on my development machine. Consider for 
#           future changes.
parser = argparse.ArgumentParser(   description="Attempts to update a Doxygen project for the \
                                                 provided Doxyfile.")

# Add options to parser with '-h|--help' support.
parser.add_argument('doxyfile',                     nargs="?",                          help="Path to Doxyfile to process.")
parser.add_argument('-s', '--silent',                           action='store_true',    help="Flag indicating that 'silent mode' was requested. Silent mode ensures there is no pause after the script finishes executing.")
parser.add_argument('-d', '--directory',            nargs=1,                            help="[OPTIONAL] Path to directory to use when searching for parameters if the provided items can not be found.")




####################################################################################################
# @brief            Wrapper function to allow passing parameters when invoking the main processing 
#                   loop from a parent script [e.g. 'update_all.py'].
# 
# @retval           0                       Successful completion of function.
# @retval           1                       Error code indicating unable to locate a valid Doxyfile.
# @retval           2                       Error code indicating unable to invoke the Doxygen 
#                                           utility.
####################################################################################################
def main(arg_list=None):
    # Grab access to global variables related to script parameters.
    global parser, args
    
    # Process all arguments provided to script.
    args = parser.parse_args(arg_list)
    
    # :: CHECK :: Validate Doxyfile provided. ::
    # >> Doxyfile not provided or is invalid. Override with default Doxyfile value:
    if not 'doxyfile' in vars(args) or args.doxyfile is None or not os.path.isfile(args.doxyfile):
        # Notify user of override.
        print("[WARNING]> The 'Update Doxygen' script requires the path to a Doxyfile to process.")
        sys.stdout.write("Searching current and parent directories for 'Doxyfile.conf' ... ")
        
        # Determine base directory to start searching upwards from.
        if args.directory != None:
            base            = os.path.realpath(args.directory[0])
        else:
            base            = os.path.dirname(os.path.realpath(__file__))
        
        # Look for Doxyfile in the current working directory and 'n-levels' above:
        flag_valid_path = False
        levels          = os.path.join("..", "..")
        lvl_cnt         = 0
        while lvl_cnt != 4:
            doxyfile = os.path.join( base, levels[0:3]*lvl_cnt, "Doxyfile.conf" )
            if os.path.isfile(doxyfile):
                flag_valid_path = True
                break
            else:
                lvl_cnt += 1
        
        # Determine absolute path or exit with NO ERROR if no valid Doxyfile determined.
        if flag_valid_path == True:
            print("SUCCESS!")
            doxyfile    = os.path.realpath(doxyfile)
            path        = os.path.dirname(doxyfile)
            fname       = os.path.basename(doxyfile)
        else:
            print("No Doxyfile located. Exiting.")
            return 1
        
    # >> Doxyfile is okay. Store absolute path and path components in global variables:
    else:
        doxyfile    = os.path.realpath(args.doxyfile)
        path        = os.path.dirname(doxyfile)
        fname       = os.path.basename(doxyfile)
    
    
    ################################################################################################
    # UPDATE DOXYGEN DOCUMENTATION.
    ################################################################################################
    # Notify user of processing step.
    print( "Will attempt to process the Doxygen project: <%s>/%s." % (str(path), str(fname)) )
    
    # Change working directory to location of <doxygen> output.
    pwd = os.getcwd()
    os.chdir(os.path.join(path))
    
    # Run Doxygen command.
    try:
        subprocess.check_call( ["doxygen", str(fname)], stderr=subprocess.STDOUT )
    except:
        print("[FATAL_ERROR]> Unable to execute Doxygen command for designated Doxyfile. Exiting.")
        return 2
    
    
    ################################################################################################
    # CLEANUP ANY TEMP FILES GENERATED DURING THE DOXYGEN PROCESS.
    ################################################################################################
    print("Searching for any temporary files from Doxygen process:")
    for root, dirs, files in os.walk(os.getcwd()):
        for f in files:
            # Break file name into components.
            name, exten = os.path.splitext(f)
            
            # :: CHECK :: Look for and remove any file ending in '.db'. ::
            if name.startswith("doxygen_") and (exten == ".db" or exten == ".tmp"):
                print( "  - Removing '%s'." % f )
                os.remove(f)
        
        # Break from loop after processing first-level.
        break
    
    # Return to original working directory and exit a success code.
    os.chdir(pwd)
    return 0




####################################################################################################
# TREAT SCRIPT AS STANDALONE IF FILE WAS NOT IMPORTED.
####################################################################################################
if __name__ == "__main__":
    # Invoke main function with all but the name of the current script as a parameter list for the 
    # argparse module.
    rc = main(sys.argv[1:])
    
    # Pause execution so long as 'silent' mode not set [flag not passed to function].
    if args.silent == False:
        raw_input("Press any key to continue ...")
    
    # Exit with exit code from main function.
    sys.exit(rc)




