####################################################################################################
# @file
# 
# @brief            Invokes all management and update scripts related to maintaining Doxygen 
#                   documentation.
# 
# @details          Invokes all scripts parallel to the directory this script resides within in a 
#                   specific order [not dynamic]. Aim is to encapsulate the process of invoking all 
#                   of the related Documentation scripts via one simple script.
#                                                                                                   \n\n
#                   This script presumes:
#                       1. There is a single 'documentation' folder [name does not matter].
#                       2. Within the first level of the documentation folder, there is a single 
#                          Doxyfile.
#                       3. The Doxyfile will generate a single <latex> directory in the root of the 
#                          documentation folder.
#                                                                                                   \n\n
#                   With the above assumptions, you may provide one optional parameter 'Doxyfile' 
#                   [path + name of file] and all other items will be determined. If no Doxyfile is 
#                   provided, the behavior is left up to the individual scripts to determine the 
#                   desired default paths/values.
# 
# @note             All scripts invoked from this script will have the optional '-s|--silent' flag 
#                   provided regardless of what is passed to this script. The status of the silent 
#                   flag will instead be used as this script exists.
# 
# @param[in]        doxyfile                Path to Doxyfile to process.
# @param[in]        flags                   Any of the supported flags:
#                                               - -s | --silent
# 
# @attention        DEPENDENCIES:
#                       - Python {2.7.9++; 3.4.2++}
#                       - Doxygen 1.8.8++
#                       - pdflatex
# 
# @retval           0                       Successful completion of script.
# @retval           1                       An error with the 'Run Customizations' script caused the 
#                                           script to abort.
# @retval           2                       An error with the 'Update Doxygen' script caused the 
#                                           script to abort.
# @retval           3                       An error with the 'Update Summary PDFs' script caused 
#                                           the script to abort.
####################################################################################################


####################################################################################################
# IMPORT MODULES
####################################################################################################
import sys, os                                                                  # General system level access/commands.
import argparse                                                                 # Adds support for processing command line arguments.

# Ensure 'press any key' will work for Python 3 instances.
if sys.version[0] >= "3":
    raw_input=input




####################################################################################################
# DEFINE COMMAND LINE ARGUMENTS
####################################################################################################
# Create parser instance with description.
# >> NOTE:  The optional 'allow_abbrev=False' to exclude partial matches is not available in the 
#           present version of Python 2.7.X accessible on my development machine. Consider for 
#           future changes.
parser = argparse.ArgumentParser(   description="Attempts to update all related documentation for \
                                                 the project associated with the provided \
                                                 Doxyfile.")

# Add options to parser with '-h|--help' support.
parser.add_argument('doxyfile',                     nargs="?",                          help="Path to Doxyfile to process.")
parser.add_argument('-s', '--silent',                           action='store_true',    help="Flag indicating that 'silent mode' was requested. Silent mode ensures there is no pause after the script finishes executing.")
parser.add_argument('-r', '--retain-files',                     action='store_true',    help="Flag indicating that 'retain files mode' was requested. Retain files mode ensures that the original LaTeX files are not removed [cleaned up].")
parser.add_argument('-d', '--directory',            nargs=1,                            help="[OPTIONAL] Path to directory to use when searching for parameters if the provided items can not be found.")




####################################################################################################
# @brief            Wrapper function to allow passing parameters when invoking the main processing 
#                   loop from a parent script [e.g. 'update_all.py'].
# 
# @retval           0                       Successful completion of function.
# @retval           1                       Error code indicating unable to locate a valid Doxyfile.
# @retval           2                       Error code indicating unable to invoke the Doxygen 
#                                           utility.
####################################################################################################
def main(arg_list=None):
    # Grab access to global variables related to script parameters.
    global parser, args
    
    # Process all arguments provided to script.
    args = parser.parse_args(arg_list)
    
    # :: CHECK :: Validate path provided. ::
    # >> Path not provided or is invalid. Override with default path value:
    if not 'doxyfile' in vars(args) or args.doxyfile is None or not os.path.isfile(args.doxyfile):
        doxyfile    = ""
        path        = ""
        fname       = ""
    # >> Doxyfile value is okay. Use to define path related components.
    else:
        doxyfile    = os.path.realpath(args.doxyfile)
        path        = os.path.dirname(doxyfile)
        fname       = os.path.basename(doxyfile)
    
    # ** TO PREVENT ERRORS, WIPE ANY ARGUMENTS PASSED TO FILE AFTER PROCESSING ARGUMENTS. **
    del sys.argv[1:]
    
    # Start temporary argument list with common 
    base_args   = []
    if args.directory != None:
        base_args.extend(["--directory", args.directory[0]])
    
    
    ################################################################################################
    # PREPARE ENVIRONMENT FOR INVOKING PYTHON SCRIPTS.
    ################################################################################################
    # Save working directory to restore before exiting the script.
    pwd = os.getcwd()
    
    # Determine directory of current file [script].
    script_dir = os.path.dirname(os.path.realpath(__file__))
    
    # Append current path to system path if not already a component.
    if not os.path.realpath(script_dir) in sys.path:
        print("Appending '%s' to Python import path." % script_dir)
        sys.path.append(script_dir)
    
    
    ################################################################################################
    # INVOKE ALL RELEVANT DOCUMENTATION SCRIPTS.
    #-----------------------------------------------------------------------------------------------
    # NOTE: If need to pass an optional flag, make sure to do so with an if-else case. Using a dummy 
    #       variable that may be empty ('') will break flow in Windows.
    ################################################################################################
    # Execute 'Run Customizations' script:
    try:
        os.chdir(script_dir)
        import run_customizations
        if run_customizations.main(base_args + [os.path.join(path, ".update_overview", "customizations")]) != 0:
            raise
    except:
        print("**** Unable to successfully execute the 'Run Customizations' script. Exiting. ****")
        return 1
    
    # Execute 'Update Doxygen' script:
    try:
        os.chdir(script_dir)
        import update_doxygen
        if update_doxygen.main(base_args + [doxyfile]) != 0:
            raise
    except:
        print("**** Unable to successfully execute the 'Update Doxygen' script. Exiting. ****")
        return 2
    
    # Execute 'Update Summary PDFs' script:
    try:
        os.chdir(script_dir)
        import update_summary_pdfs
        if args.retain_files == True:
            rc = update_summary_pdfs.main(base_args + [os.path.join(path, "latex", "refman.tex"), "--retain-files"])
        else:
            rc = update_summary_pdfs.main(base_args + [os.path.join(path, "latex", "refman.tex")])
        if rc != 0:
            raise
    except:
        print("**** Unable to successfully execute the 'Update Summary PDFs' script. Exiting. ****")
        return 3




####################################################################################################
# TREAT SCRIPT AS STANDALONE IF FILE WAS NOT IMPORTED.
####################################################################################################
if __name__ == "__main__":
    # Invoke main function with all but the name of the current script as a parameter list for the 
    # argparse module.
    rc = main(sys.argv[1:])
    
    # Pause execution so long as 'silent' mode not set [flag not passed to function].
    if args.silent == False:
        raw_input("Press any key to continue ...")
    
    # Exit with exit code from main function.
    sys.exit(rc)




