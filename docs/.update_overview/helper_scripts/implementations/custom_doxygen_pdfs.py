####################################################################################################
# @brief        Provides classes and methods to generate custom PDFs based on a Doxygen output.
# 
# @details      Expands upon the following base class(es):
#                   1. ProcessLatexFile()
#                       * Use the base methods to provide methods to generate PDFs based on the 
#                         original LaTeX file generated during Doxygen documentation processing.
# 
# @author       Michael Stephens (MCS)
# 
# @attention    DEPENDENCIES:
#                   - Python 2.7.X
####################################################################################################


####################################################################################################
## Import Modules
####################################################################################################
from __future__ import print_function                                           # Add support for update print function.
import os, sys                                                                  # Manipulate system-level functions.
import shutil                                                                   # Used to remove non-empty directory(ies) during cleanup.
from ..base_files import latex                                                  # Used to parse the Doxygen LaTeX file and generate custom LaTeX=>PDF files based on the original.




####################################################################################################
# @class        CustomLatexPDF
# 
# @brief        Class providing methods for parsing a LaTeX file and generating PDF(s) based on 
#               re-ordered/filtered/etc. components of the original LaTeX file.
####################################################################################################
class CustomLatexPDF(latex.ProcessLatexFile):
    
    
    ################################################################################################
    # @brief        Constructor for the class.
    # 
    # @note         Please see the parent class for all notes on parameters and returns.
    ################################################################################################
    def __init__(self, data):
        # Initialize parent class.
        return super(CustomLatexPDF, self).__init__(data)
    
    
    ################################################################################################
    # @brief        Generates a summary PDF with only minimal filtering of the original LaTeX file.
    #               All sections are included with additional page breaks added between sections and 
    #               the blacklist applied by the 'parse_file()' method.
    # 
    # @details      Generates a PDF file with the [optional] 'subtitles' added to the cover page. 
    #               The resulting PDF is copied to the destination indicated by the 'output_file' 
    #               parameter.
    # 
    # @param[in]    output_file             Path + file name to use for the generated PDF.
    # @param[in]    subtitles               [OPTIONAL] String or list of lines to add to the cover 
    #                                       page of the PDF. When no value provided, defaults to:
    #                                           - "Project Overview"
    # 
    # @retval       0                       Successfully generated a summary PDF.
    ################################################################################################
    def generate_full_summary_pdf(self, output_file, subtitles="Project Overview"):
        # Open LaTeX file for processing.
        self.tmp_latex_file_open(os.path.splitext(os.path.basename(output_file))[0])
        
        # Add 'cover page' section.
        self.add_cover_page_and_toc(subtitles, include_tbl_of_cont=True)
        
        # Add all sections in the LaTeX document not already accounted for.
        self.add_section_by_filter(section_blacklist=[ "required_header",
                                                        "navigation_menu",
                                                        "tableofcontents",
                                                        "cover_page",
                                                        "required_footer"
                                                    ]
        )
        
        # Add 'navigation menu'.
        self.add_navigation_menu()
        
        # Close file.
        self.tmp_latex_file_close()
        
        # Create PDF.
        self.build_pdf(output_file)
        
        return 0
    
    
    ################################################################################################
    # @brief        Generates a summary PDF composed of only the default sections in the original 
    #               LaTeX file.
    # 
    # @details      Generates a PDF file with the [optional] 'subtitles' added to the cover page.
    #               The resulting PDF is copied to the destination indicated by the 'output_file' 
    #               parameter.
    # 
    # @param[in]    output_file             Path + file name to use for the generated PDF.
    # @param[in]    subtitles               [OPTIONAL] String or list of lines to add to the cover 
    #                                       page of the PDF. When no value provided, defaults to:
    #                                           - "Default Sections"
    # 
    # @retval       0                       Successfully generated a summary PDF.
    ################################################################################################
    def generate_default_sections_pdf(self, output_file, subtitles="Default Sections"):
        # Open LaTeX file for processing.
        self.tmp_latex_file_open(os.path.splitext(os.path.basename(output_file))[0])
        
        # Add 'cover page' section.
        self.add_cover_page_and_toc(subtitles, include_tbl_of_cont=True)
        
        # Add all 'default' sections to the LaTeX document.
        self.add_section_by_filter(section_whitelist=self.get_list_of_default_sections())
        
        # Add 'navigation menu'.
        self.add_navigation_menu()
        
        # Close file.
        self.tmp_latex_file_close()
        
        # Create PDF.
        self.build_pdf(output_file)
        
        return 0
    
    
    ################################################################################################
    # @brief        Generates a summary PDF composed of all 'non default' sections in the original 
    #               LaTeX file.
    # 
    # @details      Generates a PDF file with the [optional] 'subtitles' added to the cover page.
    #               The resulting PDF is copied to the destination indicated by the 'output_file' 
    #               parameter.
    # 
    # @param[in]    output_file             Path + file name to use for the generated PDF.
    # @param[in]    subtitles               [OPTIONAL] String or list of lines to add to the cover 
    #                                       page of the PDF. When no value provided, defaults to:
    #                                           - "Custom Sections"
    # 
    # @retval       0                       Successfully generated a summary PDF.
    ################################################################################################
    def generate_custom_sections_pdf(self, output_file, subtitles="Custom Sections"):
        # Open LaTeX file for processing.
        self.tmp_latex_file_open(os.path.splitext(os.path.basename(output_file))[0])
        
        # Add 'cover page' section.
        self.add_cover_page_and_toc(subtitles, include_tbl_of_cont=True)
        
        # Determine blacklist and whitelist items.
        blacklist = self.get_list_of_default_sections()
        whitelist = self.get_list_of_all_content_related_sections()
        
        # Add all 'default' sections to the LaTeX document.
        self.add_section_by_filter( section_blacklist=blacklist, section_whitelist=whitelist )
        
        # Add 'navigation menu'.
        self.add_navigation_menu()
       
        # Close file.
        self.tmp_latex_file_close()
        
        # Create PDF.
        self.build_pdf(output_file)
        
        return 0
    
    
    ################################################################################################
    # @brief        Generates a summary PDF for each section in the original LaTeX file.
    # 
    # @details      Breaks the original LaTeX file into multiple LaTeX/PDF files where each file 
    #               summarizes a single '\section{...}' tag. The name of each section will be 
    #               added as a 'subtitle' the cover page.
    #                                                                                               \n\n
    #               Each generated PDF is copied to the destination indicated by the 
    #               'output_directory' parameter.
    # 
    # @note         The generated PDF(s) will not include a table of contents [each PDF small enough 
    #               to not warrant one].
    # 
    # @param[in]    output_directory        Path + file name to use for the generated PDF.
    # 
    # @retval       0                       Successfully generated a summary PDF.
    ################################################################################################
    def generate_individual_sections_pdf(self, output_directory):
        # Cleanup [remove] output directory if presently exists.
        if os.path.isdir(output_directory):
            shutil.rmtree(output_directory)
        
        # Determine blacklist and whitelist items.
        blacklist = self.get_list_of_default_sections()
        whitelist = self.get_list_of_all_content_related_sections()
        
        # Loop through all whitelist items and for all entries not also in the blacklist, generate a 
        # single PDF in the designated 'output_directory'.
        for name in whitelist:
            if name not in blacklist:
                # Open LaTeX file for processing.
                self.tmp_latex_file_open(name.replace(" ", "-"))
                
                # Add 'cover page' section.
                self.add_cover_page_and_toc(name, include_tbl_of_cont=False)
                
                # Add all 'default' sections to the LaTeX document.
                self.add_section_by_filter( section_whitelist=[ name ] )
                
                # Add 'navigation menu'.
                self.add_navigation_menu()
                
                # Close file.
                self.tmp_latex_file_close()
                
                # Create PDF.
                self.build_pdf( os.path.join(output_directory, (name.replace(" ", "-") + ".pdf")) )
        
        return 0




