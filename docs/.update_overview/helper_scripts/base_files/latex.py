####################################################################################################
# @brief        Provides classes and methods used to manipulate LaTeX files.
# 
# @details      Presently provides one base classes useful for interacting with LaTeX files:
#                   1. ProcessLatexFile()
#                       * Parses the LaTeX file provided during initialization to generate an 
#                         overview list detailing a line-by-line summary of the file.
#                       * Provides methods for generating summary LaTeX files based on the original 
#                         LaTeX file [adding, removing, modifying sections].
# 
# @author       Michael Stephens (MCS)
# 
# @attention    DEPENDENCIES:
#                   - Python 2.7.X
#                   - pdflatex
####################################################################################################


####################################################################################################
## Import Modules
####################################################################################################
from __future__ import print_function                                           # Add support for update print function.
import os, sys                                                                  # Manipulate system-level functions.
import time                                                                     # Used to filter out timestamps from LaTeX files.
import subprocess                                                               # Used to invoke system commands.




####################################################################################################
# @class        ProcessLatexFile
# 
# @brief        Class providing methods for parsing a LaTeX file and generating PDF(s) based on 
#               re-ordered/filtered/etc. components of the original LaTeX file.
####################################################################################################
class ProcessLatexFile(object):
    
    
    ################################################################################################
    # DEFINE CLASS GLOBALS [CONSTANTS, VARS, ETC.]
    ################################################################################################
    
    ## Constant(s) containing layout information.
    pagebreak                               = "\\newpage"                       # Starts a new page when inserted into a LaTeX document.
    linebreak                               = "\n"                              # Starts a new line when inserted into a LaTeX document.
    
    ## A list of dictionaries containing summary information on each 'command' found in a LaTeX 
    #  file. A command is considered any line that is not a comment, blank, or a continuation of a 
    #  previous command [determined by searching for closing bracket:  {...}]. Data presently 
    #  tracked in each entry in the list:
    #   - section                   // String representing section name.
    #   - command                   // List of all lines found related to the command.
    #   - is_default_section        // Flag indicating if a 'default section'. A default section is 
    #                               // one that would be generated without adding supplemental 
    #                               // documentation [e.g. markdown].
    #   - is_content_related        // Flag indicating tag correlates to the main 'body' of the 
    #                               // LaTeX file [all tags following the cover page/table of 
    #                               // contents and prior to the navigation-menu section].
    data                                    = []
    
    ## Variable(s) used to track the current LaTeX file being processed.
    dir_latex                               = None                                  # Absolute path to directory containing LaTeX file.
    fname_latex                             = None                                  # [Full] Name of LaTeX file being processed (name + extension).
    fpath_latex                             = None                                  # Absolute path to LaTeX file being processed.
    fptr_latex                              = None                                  # Pointer to temporary LaTeX used to generate a custom PDF summary.
    line_count_latex                        = 0                                     # Count of number of lines presently added to the temporary LaTeX file.
    flags_latex                             = { 
                                                "include_toc":              False,  # Flag indicating if the 'table of contents' [toc] should be included in the PDF.
                                                "include_navigation_menu":  False   # Flag indicating if the 'navigation-menu' section at the bottom of the LaTeX file should be included in the PDF.
                                            }
    
    ## Minimum number of iterations to run so that LaTeX generates all the required linking. Needed 
    #  as the table of contents will be built on the first run and the navigation menu on the third 
    #  run of processing the LaTeX file.
    pdf_numb_iterations                     = { 
                                                "default":              1,      # Number of iterations to build a barebone PDF.
                                                "table_of_contents":    2,      # Number of iterations required if a table of contents is desired.
                                                "navigation_menu":      3       # Number of iterations required if a navigation menu is desired.
                                            }
    
    
    ################################################################################################
    # @brief        Constructor for the class.
    # 
    # @note         This method will modify the internal class variable(s):
    #                   - dir_latex
    # 
    # @param[in]    latex                   [OPTIONAL] Path to LaTeX file to process. If file 
    #                                       exists, path will be passed to the 'parse_file()' 
    #                                       method.
    # 
    # @returns      Will raise a 'ValueError' if there are any issues with the parameters provided 
    #               to the method.
    ################################################################################################
    def __init__(self, latex=None):
        # :: CHECK :: Ensure path:  (1) points to an existing file; (2) is a LaTeX file. ::
        # >> IFF VALID:  Process file [extract all lines into data array].
        if latex == None:
            pass
        elif os.path.isfile(latex) and os.path.splitext(latex)[1] == ".tex":
            self.parse_file(latex)
        else:
            raise ValueError("Provided path '%s' is not a LaTeX file." % latex)
        
        # Store the working directory of the LaTeX file for future reference.
        self.dir_latex = os.path.dirname(os.path.realpath(latex))
    
    
    ################################################################################################
    # @brief        Read in a LaTeX file at the provided path and populate the class 'self.data[]'
    #               with information detailing all of the commands in the file.
    # 
    # @details      Reads in entire LaTeX file at path provided. Parses each line and:
    #                   1. Skips over:
    #                       - Blank lines.
    #                       - Lines starting with a comment [%] character.
    #                       - Lines containing one or more 'blacklist' strings.
    #                       - Lines that insert unnecessary spaces [two '\vspace{}' tags in a row 
    #                         is considered an error; expected to be caused by 'blacklist' entry 
    #                         removals].
    #                   2. Adds a single entry to the global 'self.data[]' list for every command 
    #                      located. Where:
    #                       - A command is really a line starting with '\,,,{'.
    #                           - There may actually be more than one command on a line, but the 
    #                           concept of a command entry in the data list is at the line level.
    #                       - A command ends when the closing bracket [}] is located.
    #                       - The value of the '\section{...}' or relevant grouping of commands 
    #                         [e.g. 'required_header'] is tracked for future processing reference.
    #                       - Commands with 'section' [string] values matching the 
    #                         'default_sections[]' list will be flagged for future processing 
    #                         reference.
    # 
    # @note         This method will modify the internal class variable(s):
    #                   - data
    # 
    # @param[in]    latex                   Path to LaTeX file to process.
    # @param[in]    blacklist               [Optional] List of strings check when determining if a 
    #                                       line [or command] should be excluded. If no value 
    #                                       provided, adds:  {current date; 'Generate By'}.
    # @param[in]    default_sections        [Optional] List containing the name of any 
    #                                       '\section{...}' entries consider to be a default part of 
    #                                       a generated LaTeX file. If no value provided, uses a 
    #                                       list associated with Doxygen LaTeX files.
    # 
    # @retval       0                       Successfully processed the LaTeX file.
    # @returns      Will raise a 'ValueError' if there are any issues with the parameters provided 
    #               to the method.
    ################################################################################################
    def parse_file(self, latex, blacklist=None, default_sections=None):
        # :: CHECK :: Ensure path is:  (1) point to an existing file; (2) is a LaTeX file. ::
        if not os.path.isfile(latex) or os.path.splitext(latex)[1] != ".tex":
            raise ValueError("Provided path '%s' is not a LaTeX file." % latex)
        
        # Prepare list of values to skip if line contains one of the contained substrings.
        if blacklist is not None:
            if type(blacklist) is not list or len(blacklist) > 0 and type(blacklist[0]) != str:
                raise ValueError("Provided value for 'blacklist' is an invalid type [must be a list of strings].")
        else:
            blacklist = [ 
                "Generated by ", 
                "%s %s %s %s" % (time.strftime("%a"), time.strftime("%b"), time.strftime("%d").lstrip("0"), time.strftime("%Y"))
            ]
        
        # Prepare list of 'default sections'.
        if default_sections is not None:
            if type(default_sections) is not list or len(default_sections) > 0 and type(default_sections[0]) != str:
                raise ValueError("Provided value for 'default_sections' is an invalid type [must be a list of strings].")
        else:
            default_sections = [
                "Module Index", 
                "Namespace Index", 
                "File Index", 
                "Module Documentation", 
                "Namespace Documentation", 
                "File Documentation", 
                "Hierarchical Index", 
                "Class Index", 
                "Class Documentation" 
            ]
        
        # Extract all 'raw' lines into tmp list.
        with open(latex) as f:
            tmp_lines = f.read().splitlines()
            f.close()
        
        # Cleanup/prepare data before parsing lines extracted from file.
        self.data                           = []
        section_break                       = False
        section                             = "required_header"
        line_prev                           = ""
        section_prev                        = ""
        
        # Loop through all lines and add each command as a component of a list entry in the 'data' 
        # variable for the current instance.
        for line in tmp_lines:
            # Cleanup line.
            line = line.strip()
            
            # Skip any line that matches any of the following criteria:
            #   1. Empty.
            #   2. A comment line string [starts with '%'].
            #   3. Contains an entry in the blacklist.
            #   4. Line is a repeated instance of '\vspace' [need to skip as this occurs due to 
            #      removing blacklisted lines and results in too much padding].
            if line == "" or line.startswith("%") or any(substr in line for substr in blacklist) or line.startswith("\\vspace") and line_prev.startswith("\\vspace"):
                # **** Only track section breaks after wrap up the required header! ****
                if section.lower() == "file documentation" and line == "":
                    section = "navigation_menu"
                continue
            
            # Record the current value of the line for reference on the next iteration in the loop.
            line_prev = line
            
            #---------------------------------------------------------------------------------------
            # Determine 'section' value:
            #   - Presumes file is 'required_header' until the instance of '\begin'.
            #   - Labels all content from '\begin' to the first '\section' as 'cover_page' EXCEPT:
            #   - Any line containing the label '\tableofcontents' will be skipped.
            #   - Label all section names using the value provided in the '\section{...}' command.
            #   - Presumes a blank line indicates the start of a new section.
            #   - Presumes the first blank line following the start of the 'File Documentation' 
            #     section indicates the start of the 'navigation_menu' section.
            #   - Presumes all of file following '\end' is 'required_footer'.
            #---------------------------------------------------------------------------------------
            # NOTE:  The 'navigation_menu' section is labeled above in the 'skip any lines' section.
            #---------------------------------------------------------------------------------------
            flag_is_content_related = False
            if line.startswith("\\begin{document}"):
                section                             = "cover_page"
                continue
            elif line.startswith("\\end{document}"):
                section                             = "required_footer"
                continue
            elif "\\tableofcontents" in line:
                continue
            elif line.startswith("\\chapter") or line.startswith("\\section"):
                section                             = line.split("{")[1].split("}")[0]
                flag_is_content_related             = True
            
            # Skip line IFF all of the following is true:
            #   1. Start of a new section.
            #   2. Line starts with "\\newpage".
            # Do this as we will handle inserting '\newpage' commands between sections automatically.
            if section != section_prev and line.startswith("\\newpage"):
                continue
            
            # Record the current value of the section for reference on the next iteration in the loop.
            section_prev = section
            
            # Record line in tracking list:
            #  - If line starts with a '\', it is the start of a new command.
            #  - Else, append line to previous command.
            if line.startswith("\\"):
                flag_is_default = False
                if any(name in section for name in default_sections):
                    flag_is_default = True
                self.data.append({"section": section, "command": [line], "is_default_section": flag_is_default, "is_content_related": flag_is_content_related})
            else:
                self.data[len(self.data) - 1]["command"].append(line)
            
        # Indicate successfully processed LaTeX file.
        return 0
    
    
    ################################################################################################
    # @brief        Generate a list of all content-related sections. A 'content-related section' is 
    #               any section in the 'main body' of the original LaTeX file.
    # 
    # @details      Loops through the global 'self.data[]' list and appends the section name for 
    #               each tag not considered a structural item. Each section name will be 
    #               included once overall [not once per tag].
    # 
    # @note         Will return a list type even if the list is empty. This is to prevent possible 
    #               errors by methods looking to use this result as a base and 'append/extend' it 
    #               blindly.
    # 
    # @retval       [List]                  List of all content-related section names.
    ################################################################################################
    def get_list_of_all_content_related_sections(self):
        # Loop through all entries in the class instance for 'self.data[]' and generate a list of 
        # all 'content-related sections'.
        tmp = []
        for tag in self.data:
            if tag["is_content_related"] == True and tag["section"] not in tmp:
                tmp.append(tag["section"])
        return tmp
    
    
    ################################################################################################
    # @brief        Generate a list of all sections flagged as 'default' during processing. This 
    #               can then be used as part (or all) of a whitelist or blacklist filter.
    # 
    # @details      Loops through the global 'self.data[]' list and appends the section name for 
    #               each tag flagged as 'default' during processing. Each section name will be 
    #               included once overall [not once per tag].
    # 
    # @note         Will return a list type even if the list is empty. This is to prevent possible 
    #               errors by methods looking to use this result as a base and 'append/extend' it 
    #               blindly.
    # 
    # @retval       [List]                  List of all section names that are presently flagged as 
    #                                       'default'.
    ################################################################################################
    def get_list_of_default_sections(self):
        # Loop through all entries in the class instance for 'self.data[]' and generate a list of 
        # all 'default sections'.
        tmp = []
        for tag in self.data:
            if tag["is_default_section"] == True and tag["section"] not in tmp:
                tmp.append(tag["section"])
        return tmp
    
    
    ################################################################################################
    # @brief        Opens a temporary LaTeX file used to generate a custom PDF based off of the 
    #               most recent LaTeX file parsed by this class.
    # 
    # @details      Forcibly closes any open LaTeX file pointers and deletes any existing LaTeX file 
    #               matching the file name requested within the current 'self.dir_latex' directory. 
    #               Updates the tracking information in class variables related to the temporary 
    #               LaTeX file tracking.
    # 
    # @note         This method will modify the internal class variable(s):
    #                   - fname_latex
    #                   - fpath_latex
    #                   - fptr_latex
    #                   - flags_latex
    # 
    # @param[in]    latex_fname             Name of file [with or without extension] to open and use 
    #                                       as a temporary LaTeX file for generating a custom PDF.
    # 
    # @retval       0                       Successfully opened a temporary LaTeX file.
    ################################################################################################
    def tmp_latex_file_open(self, latex_fname):
        # Ensure there are no active file pointers open.
        self.tmp_latex_file_close()
        
        # Determine the full name and path to the temporary LaTeX file based off the base name 
        # string provided to the method.
        if not latex_fname.endswith(".tex"):
            self.fname_latex    = (str(latex_fname) + ".tex")
        self.fpath_latex    = os.path.join(self.dir_latex, self.fname_latex)
        
        # Delete any existing LaTeX files matching the path and name determined above.
        if os.path.isfile(self.fpath_latex):
            os.remove(self.fpath_latex)
        
        # Clear out any flags.
        for entry in self.flags_latex:
            self.flags_latex[entry] = False
        
        # Open the requested LaTeX file.
        self.fptr_latex     = open(self.fpath_latex, 'w')
        
        # Add header section after opening the file.
        self._add_header()
        
        return 0
    
    
    ################################################################################################
    # @brief        Close any active file pointer to a temporary LaTeX file.
    # 
    # @details      Checks to see that there is indeed an active LaTeX file pointer. If one found, 
    #               closes the file and clears the tracking variables directly related to the file.
    # 
    # @warning      Not cleaning up all global 'f..._latex' vars as would break building of PDF 
    #               [need to know name and location of temporary LaTeX file to use with PDF 
    #               generating utility].
    # 
    # @note         This method will modify the internal class variable(s):
    #                   - fptr_latex
    #                   - fptr_line_count_latex
    # 
    # @retval       0                       Successfully closed a LaTeX file or no file pointer 
    #                                       needed to be closed.
    ################################################################################################
    def tmp_latex_file_close(self):
        # :: CHECK :: Ensure file pointer active before attempting to close. ::
        if self.fptr_latex != None:
            # Add footer section before closing.
            self._add_footer()
            
            # Close temporary LaTeX file and clear tracking values.
            self.fptr_latex.close()
            self.fptr_latex         = None
            self.line_count_latex   = 0
        
        return 0
    
    
    ################################################################################################
    # @brief        Adds the string or list to the temporary LaTeX file as a new line (or lines).
    # 
    # @details      Ensures there is a valid pointer to the temporary LaTeX file and appends the 
    #               'tag' provided to the method based on the type:
    #                   - Lists are joined [merged] with the class 'self.linebreak' character(s) 
    #                     and the result is appended as a string to the temporary LaTeX file.
    #                   - All non list types are typecast as strings and appended 'as is' to the 
    #                     temporary LaTeX file.
    #               
    #               In both cases, (a) 'self.linebreak' character(s) is(are) appended after adding 
    #               the 'tag' data.
    #               
    #               In addition to writing the tag data to the temporary LaTeX file, this method 
    #               will update the total number of lines written to the temp file. This is 
    #               referenced by other methods (e.g. 'add_section_by_filter()') to ensure proper 
    #               vertical spacing and breaks are accounted for based on the number of lines 
    #               written to the temp file.
    # 
    # @note         This method will modify the internal class variable(s):
    #                   - line_count_latex
    # 
    # @param[in]    tag                     List or string to append to the temp LaTeX file. When 
    #                                       type is a list, it will be joined by the 'linebreak' 
    #                                       value before appending to the file.
    # 
    # @retval       0                       Successfully added the command requested.
    # @retval       -1                      No temporary LaTeX file open for writing.
    ################################################################################################
    def add_latex_cmd(self, tag):
        # :: CHECK :: Ensure a valid file pointer to a temporary LaTeX file exists. ::
        if self.fptr_latex is None:
            return -1
        
        # Write data to temp file based on type.
        # >> For lists, merge into a string with the linebreak character(s) and:
        if type(tag) is list:
            self.fptr_latex.write(self.linebreak.join(tag) + self.linebreak)
            self.line_count_latex += len(tag)
        # >> If NOT a list, typecast as a string and write to the temp file:
        else:
            self.fptr_latex.write(str(tag) + self.linebreak)
            self.line_count_latex += 1
        
        return 0
    
    
    ################################################################################################
    # @brief        Add a section (or sections) to the temporary LaTeX file based on the section 
    #               filters provided.
    # 
    # @details      Loops through all tags in the class instance of 'self.data[]' and appends all 
    #               tags that meet both of the following criteria:
    #                   1. (IFF a non-empty blacklist) && (section name **NOT IN** the blacklist)
    #                   2. (IFF a non-empty whitelist) && (section name **IN** the whitelist)
    #               
    #               To accomplish the adding of a section, each command for a section is appended 
    #               to the temporary file in the order found within the 'self.data[]' list via the 
    #               'add_latex_cmd()' method.
    # 
    # @warning      If neither a blacklist nor a whitelist are provided, every entry in the class 
    #               instance of 'self.data[]' will be added to the temporary LaTeX file.
    # 
    # @param[in]    section_blacklist       [OPTIONAL] Blacklist
    # 
    # @retval       0                       Successfully added the section(s) passing the filters 
    #                                       provided.
    # @retval       1                       No sections matched the provided filter [none added].
    ################################################################################################
    def add_section_by_filter(self, section_blacklist=None, section_whitelist=None):
        # Loop through aggregate summary of LaTeX commands and write all commands matching the 
        # section filters provided.
        line_count_prev = self.line_count_latex
        section_prev = None
        for tag in self.data:
            # [IFF non-empty blacklist] Skip blacklist sections.
            if section_blacklist != None and any(name in tag["section"] for name in section_blacklist):
                continue
            
            # [IFF non-empty whitelist] Ensure in the whitelist for sections.
            if section_whitelist != None and not any(name in tag["section"] for name in section_whitelist):
                continue
            
            # Insert pager break before first section and between sections.
            if self.line_count_latex != 0 and (section_prev == None or section_prev != tag["section"]):
                self.add_latex_cmd(self.pagebreak)
            
            # Add tag for current section and update tracking information.
            self.add_latex_cmd(tag["command"])
            section_prev = tag["section"]
        
        # Return an error code based on the number of lines added to the temporary LaTeX file.
        if line_count_prev == self.line_count_latex:
            return 1
        else:
            return 0
    
    
    ################################################################################################
    # @brief        Add the header to the temporary LaTeX file using the tags in the class 
    #               'self.data[]' instance.
    # 
    # @details      Adds the 'required_header' section and '\begin{document}' tag to the temporary 
    #               LaTeX file using the internal 'add_section_by_filter()' and 'add_latex_cmd()' 
    #               methods.
    # 
    # @warning      This method is internal and is not intended to be called directly by any 
    #               inherited class or instance in an application using this library. It will be 
    #               called automatically by the internal 'self.tmp_latex_file_open()' method.
    # 
    # @retval       0                       Successfully added the header section to the temporary 
    #                                       LaTeX file.
    ################################################################################################
    def _add_header(self):
        # Add the 'required_header' section to the temporary LaTeX file.
        self.add_section_by_filter(section_whitelist=["required_header"])
        
        # Add the begin document tag to the temporary LaTeX file.
        self.add_latex_cmd("\\begin{document}")
        
        return 0
    
    
    ################################################################################################
    # @brief        Add the footer to the temporary LaTeX file using the tags in the class 
    #               'self.data[]' instance.
    # 
    # @details      Adds the 'required_footer' section and '\end{document}' tag to the temporary 
    #               LaTeX file using the internal 'add_section_by_filter()' and 'add_latex_cmd()' 
    #               methods.
    # 
    # @warning      This method is internal and is not intended to be called directly by any 
    #               inherited class or instance in an application using this library. It will be 
    #               called automatically by the internal 'self.tmp_latex_file_close()' method.
    # 
    # @retval       0                       Successfully added the footer section to the temporary 
    #                                       LaTeX file.
    ################################################################################################
    def _add_footer(self):
        # Add the 'required_header' section to the temporary LaTeX file.
        self.add_section_by_filter(section_whitelist=["required_footer"])
        
        # Add the end document tag to the temporary LaTeX file.
        self.add_latex_cmd("\\end{document}")
        
        return 0
    
    
    ################################################################################################
    # @brief        Adds the navigation menu to the temporary LaTeX file. [Section containing links 
    #               to the pages and sections within the document. Typically viewable on left or 
    #               right side of documents in a PDF viewer.]
    # 
    # @details      Adds the 'navigation_menu' section to the temporary LaTeX file using the 
    #               internal 'add_section_by_filter()' method.
    # 
    # @note         This method will modify the internal class variable(s):
    #                   - flags_latex
    # 
    # @retval       0                       Successfully added the navigation menu to the temporary 
    #                                       LaTeX file.
    ################################################################################################
    def add_navigation_menu(self):
        # Add the 'navigation menu' to the temporary LaTeX file.
        self.add_section_by_filter(section_whitelist=["navigation_menu"])
        
        # Update tracking flag.
        self.flags_latex["include_navigation_menu"] = True
        
        return 0
    
    
    ################################################################################################
    # @brief        Adds the cover page and [optional] table of contents to the LaTeX file. If any 
    #               subtitle(s) is(are) provided, they will be added to the end of the command list 
    #               just before the '\end{titlepage}' tag.
    # 
    # @details      Adds the 'cover_page' section and [optional] '\tableofcontents' tag to the 
    #               temporary LaTeX file using the internal 'add_section_by_filter()' and 
    #               'add_latex_cmd()' methods.
    #                                                                                               \n\n
    #               When adding the 'cover_page' section, the optional 'subtitles' will be added 
    #               based on type. Where:
    #                   - Lists are added with an additional '\vspace...' tag between entries.
    #                   - All non list types are typecast as strings and appended 'as is' to the 
    #                     temporary LaTeX file.
    # 
    # @note         To add a table of contents, both of the following criteria MUST be met:
    #                   1. A '\tableofcontents' tag must have been found while processing the base 
    #                      LaTeX file.
    #                   2. The optional 'include_tbl_of_cont' parameter must be 'True'.
    # 
    # @note         This method will modify the internal class variable(s):
    #                   - flags_latex
    # 
    # @warning      If you are opting to include a table of contents, please be aware no additional 
    #               processing will be done by this method to construct a truly accurate table. 
    #               Rather, the available table will be used 'as is'. This may result in missing 
    #               links [e.g. Doxygen partial summary PDFs].
    # 
    # @param[in]    subtitles               [OPTIONAL] List or string of lines to include in the 
    #                                       cover page. When no value provided, no additional 
    #                                       subtitles will be added.
    # @param[in]    include_tbl_of_cont     [OPTIONAL] Flag indicating that the '\tableofcontents' 
    #                                       section should be added to the temporary LaTeX file.
    # 
    # @retval       0                       Successfully added the cover page section to the 
    #                                       temporary LaTeX file.
    ################################################################################################
    def add_cover_page_and_toc(self, subtitles=None, include_tbl_of_cont=False):
        # Loop through all tag data and process further if the 'section' is 'cover_page'.
        flag_subtitles_processed    = False
        for tag in self.data:
            if tag["section"] == "cover_page":
                # Insert any 'subtitle(s)' just prior to adding the '\end{center}' or 
                # '\end{titlepage}' command [whichever comes first].
                if ( (tag["command"][0].startswith("\\end{center}")) 
                     or (flag_subtitles_processed == False and tag["command"][0].startswith("\\end{titlepage}"))
                   ):
                    flag_subtitles_processed = True
                    if subtitles != None:
                        # >> Append list entries with '\vspace{...}' separator inserted between 
                        #    entires:
                        if type(subtitles) is list:
                            tmp = []
                            for s in subtitles:
                                tmp.append("{\\large %s}\\\\" % s)
                                tmp.append("\\vspace*{0.5cm}")
                            self.add_latex_cmd(tmp)
                        
                        # Append all other items as typecast string:
                        else:
                            self.add_latex_cmd( ("{\\large %s}\\\\" % str(subtitles)) )
                    
                # Append current command to the temporary LaTeX file.
                self.add_latex_cmd(tag["command"])
                
                # Append table of contents after closing the title [cover] page and before any 
                # additional commands are added to the temporary LaTeX file.
                if tag["command"][0].startswith("\\end{titlepage}"):
                    if include_tbl_of_cont == True:
                        self.flags_latex["include_toc"] = True
                        self.add_latex_cmd(["\\tableofcontents"])
        
        return 0
    
    
    ################################################################################################
    # @brief        Generates a PDF file using the temporary LaTeX file.
    # 
    # @details      Uses the 'pdflatex' utility to generate a PDF file in the directory containing 
    #               the original and temporary LaTeX files. Once generated, copies and renames 
    #               the generated PDF to the requested 'dst_fpath' name + location value.
    #                                                                                               \n\n
    #               Before copying over the generated PDF to the final location, method:
    #                   1. Deletes any pre-existing file matching the path + file name provided.
    #                   2. Generates any missing directories required for the PDF.
    # 
    # @warning      Method requires the 'pdflatex' utility to work properly.
    # 
    # @param[in]    dst_fpath               Path + file name to use as the final destination for 
    #                                       the generated PDF.
    # 
    # @retval       0                       Successfully created a custom PDF file from the 
    #                                       temporary LaTeX file.
    # @retval       1                       Call to method used to generate PDF returned an error.
    ################################################################################################
    def build_pdf(self, dst_fpath):
        # Generate PDF summary covering full overview of documentation.
        try:
            # Determine how many iterations of processing the temporary LaTeX file are required.
            if  self.flags_latex["include_navigation_menu"] == True:
                iterations = self.pdf_numb_iterations["navigation_menu"]
            elif self.flags_latex["include_toc"] == True:
                iterations = self.pdf_numb_iterations["table_of_contents"]
            else:
                iterations = self.pdf_numb_iterations["default"]
            
            # Process the LaTeX file the number of required iterations.
            count = 1
            while count <= iterations:
                print( "    - Processing '%s' file [count %d of %d] ... " % (os.path.basename(self.fpath_latex), count, iterations) )
                subprocess.check_call( ["pdflatex", str(self.fpath_latex)], stderr=subprocess.STDOUT )
                count += 1
        
        except:
            return 1
        
        # Determine all path related local variables for copying the generated PDF.
        src_fname   = os.path.splitext(self.fname_latex)[0] + ".pdf"
        src_fpath   = os.path.join(self.dir_latex, src_fname)
        dst_fpath   = os.path.realpath(dst_fpath)
        dst_dir     = os.path.dirname(os.path.realpath(dst_fpath))
        dst_fname   = os.path.basename(dst_fpath)
        
        # Remove any existing file before copying.
        if os.path.exists( dst_fpath ):
            os.remove( dst_fpath )
        
        # Ensure destination directory exists.
        sys.stdout.write("Ensuring destination directory for generated PDF exists ... ")
        if not os.path.isdir(dst_dir):
            os.makedirs(dst_dir)
            print("Missing Directory(ies) Created.")
        else:
            print("Path Validated.")
        
        # Notify user of status
        print("Will copy the generated PDF:")
        print("  - %s" % src_fpath)
        print("To the provided destination location:")
        print("  - %s" % dst_fpath)
        
        # Copy over generated file to destination.
        os.rename( src_fpath, dst_fpath )
        
        return 0




