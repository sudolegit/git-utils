####################################################################################################
# @brief        Provides classes and methods used to generate table summaries. Intended to 
#               be paired with the 'lib__summarize_doxygen_comments.py' script.
# 
# @details      Presently provides three base classes useful for generating summaries:
#                   1. Formatting()
#                       * Provides general formatting constants and methods.
#                       * Base class inherited by all other classes in this file.
#                   2. SummaryTable()
#                       * Enables generation of table summaries on a data set.
#                       * Contains ability to output table, but does not define the core 
#                         '_prep_data()' implementation [left to children to do that as case-
#                         specific].
#                   3. SummaryPage()
#                       * Enables generation of a single or multi-page in depth summary of entries 
#                         in a data set.
#                       * Contains ability to output a single or multi-page layout, but core 
#                         implementation for the contents of a 'page' are left to children [case-
#                         specific].
# 
# @author       Michael Stephens (MCS)
# 
# @attention    DEPENDENCIES:
#                   * Python 2.7.X
####################################################################################################


####################################################################################################
## Import Modules
####################################################################################################
from __future__ import print_function                                           # Add support for update print function.
import os, sys                                                                  # Manipulate system-level functions.
import textwrap                                                                 # Used to format output text [wrap and align].
import re                                                                       # Used when analyzing input strings.




####################################################################################################
# @class        Formatting
# 
# @brief        Minimal class to define formatting used by summary classes [line breaks, whitespace, 
#               max-cols, etc.].
####################################################################################################
class Formatting(object):
    
    
    ################################################################################################
    # DEFINE CLASS GLOBALS [CONSTANTS, VARS, ETC.]
    ################################################################################################
    
    ## Character(s) used to indicate visual separation between lines and pages.
    line_break                              = '\r\n'                                                # Character(s) used to create a line break.
    paragraph_break                         = None                                                  # Consecutive line breaks grouped to create separation between paragraphs.
    section_break                           = None                                                  # Consecutive line breaks grouped to create separation between sections.
    section_divider                         = " <hr> "                                              # Character(s) used to draw dividing lines between sections in Doxygen [HTML] output.
    page_break                              = " \\latexonly \\newpage \\endlatexonly "              # Character(s) used to indicate the start of a new page.
    
    ## Character(s) used when formatting an ASCII table cell.
    char_tbl_cell_delimiter                 = "|"                                                   # Character(s) used to delimit the start and end of a table cell.
    tbl_cell_padding                        = 2                                                     # Padding before any after table cell delimiters.
    
    ## Character(s) used to define page dimensions.
    max_cols                                = 80                                                    # Maximum columns allowed per page.
    
    ## Structure wrapper used when generating summaries.
    wrapper                                 = None
    
    ## Amount to indent when wrapping paragraphs, bullet points, etc. For bullet points, represents 
    #  the amount to indent for each additional level [level one == 1*indent_width; level two == 
    #  2*indent_level; etc.].
    indent_width                            = 4
    
    ## Define the list of supported bullet point markers.
    bullet_point_markers                    = [ "-", "*", "-#" ]
    
    
    ################################################################################################
    # @brief        Constructor for the class.
    ################################################################################################
    def __init__(self):
        # Update spacing variables defined as multiples of 'line breaks'.
        self.paragraph_break                = self.line_break * 2               # Insert one line of spacing between current line of text and next line of text.
        self.section_break                  = self.line_break * 4               # Insert four lines of spacing between current line of text and next line of text.
        
        # Define remaining bullet point markers.
        self.bullet_point_markers.extend([("%s." % str(chr(x))) for x in range(65,123)])
        self.bullet_point_markers.extend([("%d." % x) for x in range(0,1000)])
        
        # Set class instance for wrapper class.
        self.wrapper                        = textwrap.TextWrapper()
    
    
    ################################################################################################
    # @brief        Wraps string provided at the column width indicated by inserting line breaks
    #               at just prior to any words that would exceed the column limit.
    # 
    # @details      Uses the 'textwrap' module to wrap text at the provided width. The results are 
    #               then merged with line breaks inserted between text-groupings.
    # 
    # @param[in]    line                    String to wrap.
    # @param[in]    width                   [OPTIONAL] Max number of characters allowed per line 
    #                                       before text should be wrapped.
    # @param[in]    initial_indent          [OPTIONAL] How much to indent first line of text when 
    #                                       wrapping. Defaults to none [""].
    # @param[in]    subsequent_indent       [OPTIONAL] How much to indent all lines after the first 
    #                                       line of text when wrapping. Defaults to none [""].
    # 
    # @retval       String                  String with line breaks inserted to achieve wrapping.
    ################################################################################################
    def wrap(self, line, width=85, initial_indent="", subsequent_indent=""):
        # Update wrapper settings.
        self.wrapper.width                  = width
        self.wrapper.initial_indent         = initial_indent
        self.wrapper.subsequent_indent      = subsequent_indent
        
        # Wrap and merge results of wrap with line breaks.
        return str(self.line_break.join(self.wrapper.wrap(line)) + self.line_break)
       
        


####################################################################################################
# @class        SummaryTable
# 
# @brief        Base class which provides methods for generating a summary tables on a dataset.
# 
# @details      The main aim of this class to provide a consistent manner for generating table  
#               summaries of a data set. By default, it will not generate any tables, but it will 
#               with minimal development by a child class or by manually setting the required 
#               variables before generating a summary.
# 
# @warning      Class handles the core work involved in outputting a table, but relies on overloads 
#               or child class development to provide a valid data set to summarize.
# 
# @note         This class is intended to be paired with the output generated by the 
#               'SummarizeDoxgenComments' class.
####################################################################################################
class SummaryTable(Formatting):
    
    
    ################################################################################################
    # DEFINE CLASS GLOBALS [CONSTANTS, VARS, ETC.]
    ################################################################################################
    
    ## Global reference of list containing Doxygen summary data. Set once during class 
    #  initialization. Should match the output formatting from the 'SummarizeDoxgenComments' class.
    data                                    = None
    
    ## Two dimensional list containing data to summarize when generating table. Should be populated 
    #  manually or by a 'prep' method. Each entry in the 'table_data' list should be itself a list 
    #  which represents a single row of data for a table. It is important that rows be of the same 
    #  dimension.
    table_data                              = None
    
    ## List containing names of columns used in table header. The length of this list must match 
    #  the length of the 'col_widths' list.
    col_names                               = None
    
    ## List of column dimensions. Defines the overall width [number of columns] for the table and 
    #  the number of characters each row requires. Dimensions should be indicated in number of 
    #  characters and should not include column separators or cell padding. The length of this list 
    #  must match the length of the 'col_names' list.
    col_widths                              = None
    
    
    ################################################################################################
    # @brief        Constructor for the class.
    # 
    # @param[in]    data                    List containing dictionary(ies) summarizing a Doxygen 
    #                                       project. The format should match that of 
    #                                       'SummarizeDoxgenComments().data'.
    # 
    # @returns      Nothing, but will raise an exception if there is an error with the values 
    #               provided.
    ################################################################################################
    def __init__(self, data):
        # Initialize parent class.
        super(SummaryTable, self).__init__()
        
        # Validate 'data' and update class instance reference.
        if data is not None and len(data) > 0 and isinstance(data, list) and isinstance(data[0], dict) and len(data[0]) > 0 and isinstance(data[0][list(data[0].keys())[0]], list):
            self.data = data
        else:
            raise ValueError("Provided value for 'data' must be a list of dictionaries where each dictionary key contains a list of values.")
    
    
    ################################################################################################
    # @brief        Loop through data entries and characterize components for use when laying 
    #               out a table summary
    # 
    # @details      Loops through 'self.data' and structure 'self.table_data', 
    #               and 'self.col_names'. When function exists, all rows in  'self.table_data' will 
    #               have the same length [number of columns' with 'None' as a value if the cell 
    #               should not be populated when drawn.
    # 
    # @note         This method is called automatically by the 'generate...()' method(s).
    # 
    # @warning      By default, this function resets/clears any tracking variables. Overloading 
    #               classes should structure this for their use case.
    # 
    # @retval       0                       Successfully prepared table data.
    # @retval       -1                      Error [no table data prepped].
    ################################################################################################
    def _prep_table_data(self):
        # Default:  Wipe all tracking data.
        self.table_data = None
        self.col_widths = None
        self.col_names  = None
        
        # Return error by default. Child classes should properly populate the above vars in an 
        # overload and return '0' to indicate success.
        return -1
    
    
    ################################################################################################
    # @breif        Structures a string containing the title row and corresponding dividing 
    #               line. Use the string returned as the start of the output for a table.
    # 
    # @details      Loops through the 'self.col_widths' list to structure the table header. The 
    #               'column widths' variable defines the loop so that in the event no the 'column 
    #               names' list is too short [or not defined at all], the correct number of columns 
    #               are defined in the header. If no definition is provided for one or more columns, 
    #               the column name will be "Col-##".
    #                                                                                               \n\n
    #               Method structures two temp strings while looping through the total number of 
    #               columns. The 'title' string represents the title row for the table; the 
    #               'divider' string represents the row immediately following the title row 
    #               containing only table cell dividers and dashes. 
    # 
    # @note         Formatting is specifically targeted at markdown tables compatible with Doxygen.
    # 
    # @retval       Table-Header            String containing table header information [both the 
    #                                       title row and the dividing line below it].
    ################################################################################################
    def _structure_markdown_table_header(self):
        # Define local variable(s) used in function.
        title                               = ""                                # [TEMP] String containing title information.
        divider                             = ""                                # [TEMP] String containing divider information.
        
        # Loop through and structure table header.
        count = 0
        for x in range(len(self.col_widths)):
            # Increment count used when no name provided.
            count += 1
            
            # Ensure name for column exists before using value.
            if self.col_names != None and len(self.col_names) >= count:
                name = self.col_names[x]
            else:
                name = "COL-" + str(count).zfill(2)
            
            # Structure temp string for column name. (Need to reference when calculating padding.)
            tmp = (" " * self.tbl_cell_padding) + name + (" " * self.tbl_cell_padding)
            
            # Append column data to tracking vars.
            title   += self.char_tbl_cell_delimiter + tmp + " " * ((self.col_widths[x] + (2 * self.tbl_cell_padding)) - len(tmp))
            divider += self.char_tbl_cell_delimiter + "-" * ((self.col_widths[x] + (2 * self.tbl_cell_padding)))
        
        # Finalize structure for title and divider lines.
        title   += self.char_tbl_cell_delimiter + self.line_break
        divider += self.char_tbl_cell_delimiter + self.line_break
        
        # concatenate and return strings.
        return str(title + divider)
        
        
    ################################################################################################
    # @breif        Structures a string containing the all contents of the table summary.
    # 
    # @details      Loops through the 'self.table_data' list to structure the table contents.
    #                                                                                               \n\n
    #               Method structures a temp string while looping through the total number of 
    #               entries [rows] in the 'table data'. The final string is then returned to the 
    #               caller function.
    # 
    # @note         Formatting is specifically targeted at markdown tables compatible with Doxygen.
    # 
    # @retval       Table-Contents          String containing table contents. If there is a 
    #                                       structure error [such as mismatched number of columns], 
    #                                       the string will be empty.
    ################################################################################################
    def _structure_markdown_table_contents(self):
        # Define local variable(s) used in function.
        contents                            = ""                                # [TEMP] String containing contents of table.
        
        # :: CHECK :: Validate length of table data rows. ::
        tmp_min_len = len(min(self.table_data, key=len))
        tmp_max_len = len(max(self.table_data, key=len))
        if tmp_min_len != tmp_max_len or tmp_max_len != len(self.col_widths):
            return
        
        # Loop through rows within a table and all columns within each row.
        for row in self.table_data:
            for x in range(len(self.col_widths)):
                # >> Add contents of existing cell:
                if len(row) >= (x + 1):
                    tmp = self.char_tbl_cell_delimiter + (" " * self.tbl_cell_padding) + row[x] + (" " * self.tbl_cell_padding)
                # >> Cell is not used by current row. Append delimiter only:
                else:
                    tmp = self.char_tbl_cell_delimiter
                
                # Add extra spacing to keep alignment between rows in table.
                contents += tmp + " " * ( (self.col_widths[x] + len(self.char_tbl_cell_delimiter) + (2 * self.tbl_cell_padding)) - len(tmp) )
            
            # Append one last delimiter and line break for table.
            contents += self.char_tbl_cell_delimiter + self.line_break
        
        # Return string to caller.
        return contents
    
    
    ################################################################################################
    # @brief        Returns a string containing any footnotes to append to the table.
    # 
    # @warning      By default, this method is simply a placeholder. Child instances that inherit 
    #               this class may opt to overload this method if footnote support is desired.
    ################################################################################################
    def _structure_table_footnotes(self):
        pass
    
    
    ################################################################################################
    # @brief        Provides command list summary in an ASCII table form.
    # 
    # @details      Processes the global 'data' list and converts each entry in the list to a 
    #               single entry in the ASCII table. The output table is in the format:
    #                                                                                               \n\n
    #                   [TABLE-NAME]                                                                \n
    #                   |  self.column_names[0]   |  self.column_names[1]   |  ...  |               \n
    #                   |-------------------------|-------------------------|-------|               \n
    #                   |  self.table_data[0][0]  |  self.table_data[0][1]  |  ...  |               \n
    #                   ...                                                                         \n
    #                   |  self.table_data[1][0]  |  self.table_data[1][1]  |  ...  |               \n
    #                                                                                               \n\n
    #
    # @param[in]    table_name              [OPTIONAL] Name of table being generated [text header 
    #                                       for table].
    # @param[in]    trailing_page_break     [OPTIONAL] Flag indicating that there should always be 
    #                                       a page break inserted before a new page. Defaults to 
    #                                       'False'.
    # 
    # @note         Presumes the 'self.table_data[]' has been processed. If the 'self.col_names[]' 
    #               list is empty, generic header names in the form 'COL-##' will be used.
    # 
    # @returns      String containing full summary generated by processing the class instance 
    #               'data' variable.
    ################################################################################################
    def generate(self, table_name=None, trailing_page_break=False):
        
        # Define local variable(s) used in function.
        output                              = ""                                # String containing formatted output.
        
        
        ############################################################################################
        # PROCESS DATA FOR USE IN GENERATING TABLE SUMMARY.
        ############################################################################################
        if self._prep_table_data() != 0:
            return
        
        
        ############################################################################################
        # DETERMINE COLUMN DIMENSIONS FOR TABLE.
        ############################################################################################
        self.col_widths = []
        for row in self.table_data:
            for col_index in range(len(row)):
                # >> First time for column. Take maximum of:  (current width, title width).
                if len(self.col_widths) < (col_index + 1):
                    self.col_widths.append(max( len(row[col_index]), len(self.col_names[col_index]) ))
                # >> Column exists. Take max possible cell width:
                else:
                    self.col_widths[col_index] = max( self.col_widths[col_index], len(row[col_index]) )
        
        
        ############################################################################################
        # APPEND OPTIONAL TABLE TITLE.
        ############################################################################################
        if table_name is not None:
            output += str(table_name) + (self.line_break * 2)
        
        
        ############################################################################################
        # APPEND TABLE HEADER AND DIVIDING LINE BELOW THE HEADER.
        ############################################################################################
        tmp = self._structure_markdown_table_header()
        if tmp == None or tmp == "":
            return
        output += str(tmp)
        
        
        ############################################################################################
        # APPEND TABLE CONTENTS.
        ############################################################################################
        tmp = self._structure_markdown_table_contents()
        if tmp == None or tmp == "":
            return
        output += str(tmp)
        
        
        ############################################################################################
        # APPEND OPTIONAL TABLE FOOTNOTES.
        ############################################################################################
        output += self._structure_table_footnotes()
        
        
        ############################################################################################
        # INSERT TRAILING PAGE BREAK.
        ############################################################################################
        if trailing_page_break == True:
            output += self.page_break + self.paragraph_break
        
        
        ############################################################################################
        # RETURN OUTPUT STRING.
        ############################################################################################
        return output




####################################################################################################
# @class        SummaryPage
# 
# @brief        Provides methods for generating page summaries for commands within a provided 
#               dataset.
# 
# @details      The main aim of this class to provide a consistent manner for generating an appendix 
#               style summary of a command or group of available commands in a data set. It is 
#               intended to be paired with the output generated by the 'SummarizeDoxgenComments' 
#               class.
# 
# @warning      Class handles the general structural work involved in generating a page summary, but 
#               it relies on overloads or child class development to structure a valid [single] page 
#               summary on an entry in a data set.
####################################################################################################
class SummaryPage(Formatting):
    
    
    ################################################################################################
    # DEFINE CLASS GLOBALS [CONSTANTS, VARS, ETC.]
    ################################################################################################
    
    ## Global reference of list containing Doxygen summary data. Set once during class 
    #  initialization. Should match the output formatting from the 'SummarizeDoxgenComments' class.
    data                                    = None 
    
    
    ################################################################################################
    # @brief        Constructor for the class.
    # 
    # @param[in]    data                    List containing dictionary(ies) summarizing a Doxygen 
    #                                       project. The format should match that of 
    #                                       'SummarizeDoxgenComments().data'.
    # 
    # @returns      Nothing, but will raise an exception if there is an error with the values 
    #               provided.
    ################################################################################################
    def __init__(self, data):
        # Initialize parent class.
        super(SummaryPage, self).__init__()
        
        # Validate 'data' and update class instance reference.
        if data is not None and len(data) > 0 and isinstance(data, list) and isinstance(data[0], dict) and len(data[0]) > 0 and isinstance(data[0][list(data[0].keys())[0]], list):
            self.data = data
        else:
            raise ValueError("Provided value for 'data' must be a list of dictionaries where each dictionary key contains a list of values.")
    
    
    ################################################################################################
    # @brief        Converts the lines provided into a paragraph representation via word-wrapping 
    #               at the max column width defined in the 'Formatting()' class.
    # 
    # @details      Loops through the provided list and treats each entry in the list as an 
    #               individual paragraph. Each paragraph is word-wrapped and extra spacing is 
    #               inserted before adding paragraphs [starting at the second paragraph] to ensure 
    #               good visual separation.
    # 
    # @param[in]    lines                   List to process and convert into the contents of the 
    #                                       section.
    # @param[in]    indent                  [OPTIONAL] Flag indicating if paragraphs should respect 
    #                                       the indentation level. Defaults to 'True'.
    # 
    # @note         The input variable 'lines' is presumed to contain valid data. All entries in  
    #               the list will be included in the contents of the section.
    # 
    # @note         All paragraphs will be indented by the amount indicated in 'self.indent_width'.
    # 
    # @retval       String                  Formatted 'lines[]' in paragraph representation.
    ################################################################################################
    def _merge_list_as_paragraphs(self, lines, indent=True):
        # Define local variable(s) used in function.
        summary                             = ""                                # String containing formatted output.
        count                               = 0                                 # Tracking variable for current line being printed.
        
        # Define indentation width based on indent flag provided to method.
        if indent == True:
            indent_width = self.indent_width
        else:
            indent_width = 0
        
        # Loop through all elements in the provided list and word-wrap each entry in the list as an 
        # individual paragraph.
        for entry in lines:
            # Increment count and look to insert an additional break.
            count += 1
            if count > 1:
                summary += self.line_break
            
            # Split each entry by line characters and wrap them
            tmp = entry.splitlines()
            for line in tmp:
                summary += self.wrap(line, initial_indent=(" " * indent_width), subsequent_indent=(" " * indent_width))
        
        # Return summary
        return summary
    

    ################################################################################################
    # @brief        Converts the lines provided into an ordered [numbered] bullet point 
    #               representation and word-wrapps at the max column width defined in the 
    #               'Formatting()' class for each bullet point.
    # 
    # @details      Loops through the provided list and executes one of the following process 
    #               options on each list item:
    #                   1. If more than one space [' '] between first and second word:
    #                       * Write first word as level-01, ordered bullet point [#.].
    #                       * Strip spaces between first and second word.
    #                       * Word-wrap all remaining words in entry as level-02, ordered bullet 
    #                         point [1.].
    #                   2. No extra spacing detected:
    #                       * Word-wrap entire entry as level-02, ordered bullet point [#.]
    # 
    # @param[in]    lines                   List to process and convert into the contents of the 
    #                                       section.
    # 
    # @note         The input variable 'lines' is presumed to contain valid data. All entries in  
    #               the list will be included in the contents of the section.
    # 
    # @note         Each bullet point level will be a linear multiple of the amount indicated in 
    #               'self.indent_width' [first level == 1 * indent_width; second level == 2 * 
    #               indent_width; etc.].
    # 
    # @retval       String                  Formatted 'lines[]' in bullet point notation.
    ################################################################################################
    def _merge_list_as_ordered_bullet_points(self, lines):
        # Define local variable(s) used in function.
        summary                             = ""                                # String containing formatted output.
        count                               = 0                                 # Tracking variable for current line being printed.
        
        # Loop through all elements in the provided list and convert each entry in the list to an 
        # ordered list.
        for entry in lines:
            # Increment counter and prep indent level string.
            count += 1
            indent = "%s%d. " % ((" " * self.indent_width), count)
            
            # Analyze spaces between words to see if first word should be on its own line.
            spaces = re.findall(r'\s+', entry)
            
            # :: CHECK :: Determine if want to use two bullet point levels or one based on the 
            #             contents of the 'spaces' list. ::
            # >> Split into two levels with the first level numbered according to the initial 
            #    indent string determined above and the second level fixed at '#1':
            if len(spaces) > 0 and len(spaces[0]) > 1:
                tmp = entry.split(" ")
                summary += self.wrap(str(tmp[0]).strip(),       initial_indent=indent,  subsequent_indent=(" " * len(indent)) )
                indent = "%s%d. " % ((" " * self.indent_width * 2), 1)
                summary += self.wrap(" ".join(tmp[1:]).strip(), initial_indent=indent,  subsequent_indent=(" " * len(indent)) )
            
            # >> Only one bullet point level desired. Use the indent value above and word-wrap:
            else:
                summary += self.wrap(str(entry).strip(),    initial_indent=indent,  subsequent_indent=(" " * len(indent)) )
        
        return summary
    
    
    ################################################################################################
    # @brief        Converts the lines provided into an unordered bullet point representation and 
    #               word-wraps at the max column width defined in the 'Formatting()' class for each 
    #               bullet point.
    # 
    # @details      Loops through the provided list and executes one of the following process 
    #               options on each list item:
    #                   1. If more than one space [' '] between first and second word:
    #                       * Write first word as level-01, unordered bullet point [*].
    #                       * Strip spaces between first and second word.
    #                       * Word-wrap all remaining words in entry as level-02, ordered bullet 
    #                         point [*].
    #                   2. No extra spacing detected:
    #                       * Word-wrap entire entry as level-02, ordered bullet point [*]
    # 
    # @param[in]    lines                   List to process and convert into the contents of the 
    #                                       section.
    # 
    # @note         The input variable 'lines' is presumed to contain valid data. All entries in  
    #               the list will be included in the contents of the section.
    # 
    # @note         Each bullet point level will be a linear multiple of the amount indicated in 
    #               'self.indent_width' [first level == 1 * indent_width; second level == 2 * 
    #               indent_width; etc.].
    # 
    # @retval       String                  Formatted 'lines[]' in bullet point notation.
    ################################################################################################
    def _merge_list_as_unordered_bullet_points(self, lines):
        # Define local variable(s) used in function.
        summary                             = ""                                # String containing formatted output.
        
        # Loop through all elements in the provided list and convert each entry in the list to an 
        # unordered list.
        for entry in lines:
            # Prep initial indent string.
            indent = "%s* " % (" " * self.indent_width)
            
            # Analyze spaces between words to see if first word should be on its own line.
            spaces = re.findall(r'\s+', entry)
            
            # :: CHECK :: Determine if want to use two bullet point levels or one based on the 
            #             contents of the 'spaces' list. ::
            # >> Split into two levels with both using '*' for the bullet point indicator, but the 
            #    second level indented further than the first based as a multiple of the 
            #    'self.indent_width' value.
            if len(spaces) > 0 and len(spaces[0]) > 1:
                tmp = entry.split(" ")
                summary += self.wrap(str(tmp[0]).strip(),       initial_indent=indent,  subsequent_indent=(" " * len(indent)) )
                indent = "%s* " % (" " * self.indent_width * 2)
                summary += self.wrap(" ".join(tmp[1:]).strip(), initial_indent=indent,  subsequent_indent=(" " * len(indent)) )
            
            # >> Only one bullet point level desired. Use the indent value above and word-wrap:
            else:
                summary += self.wrap(str(entry).strip(),        initial_indent=indent,  subsequent_indent=(" " * len(indent)) )
        
        return summary
    
    
    ################################################################################################
    # @brief        Converts the data[] list into a paragraph representation via word-wrapping at 
    #               the max column width defined in the 'Formatting()' class.
    # 
    # @details      Mainly serves as auto-wrapper to other formatting methods within this class:
    #                   * If neither ordered or unordered list items are flagged:
    #                       * Invokes method to summarize as paragraph(s).
    #                   * If ordered list is enabled [regardless of state of unordered list  flag]:
    #                       * Invokes method to summarize as ordered list.
    #                   * If unordered list is enabled [and unordered is NOT enabled]:
    #                       * Invokes method to summarize as unordered list.
    #               
    #               After generating summary, the 'title' will be prepended so long as:
    #                   1. The summary returned a non-empty string.
    #                   2. The title is a non-empty string.
    # 
    # @param[in]    title                   Name of section being summarized.
    # @param[in]    lines                   List to process and convert into the contents of the 
    #                                       section.
    # @param[in]    ordered_list            [OPTIONAL] Flag indicating if an ordered list summary 
    #                                       is desired. Defaults to 'False'.
    # @param[in]    unordered_list          [OPTIONAL] Flag indicating if an unordered list summary 
    #                                       is desired. Defaults to 'False'.
    # @param[in]    treat_as_code           [OPTIONAL] Flag indicating if the lines should be 
    #                                       treated as a code sample and wrapped with the relevant 
    #                                       Doxygen tags. Defaults to 'False'.
    # 
    # @note         The input variable 'lines' is presumed to contain valid data. All entries in  
    #               the list will be included in the contents of the section.
    # 
    # @warning      When selecting list options, you may only select 'ordered' or 'unordered'. If 
    #               both flags are set, the list will be 'ordered'.
    # 
    # @retval       String                  Summary of section content [title + string].
    ################################################################################################
    def _add_section(self, title, lines, ordered_list=False, unordered_list=False, treat_as_code=False):
        # Add  section summary as paragraph or list based on flags provided.
        # >> Paragraph summary:
        if ordered_list is False and unordered_list is False:
            if str(title) == "":
                section = self._merge_list_as_paragraphs(lines, indent=False)
            else:
                section = self._merge_list_as_paragraphs(lines, indent=True)
        # >> Ordered [numbered] list summary:
        elif ordered_list is True:
            section = self._merge_list_as_ordered_bullet_points(lines)
        # >> Unordered [bulleted] list summary:
        elif unordered_list is True:
            section = self._merge_list_as_unordered_bullet_points(lines)
        
        # Wrap section with '@code [seciton] @endcode' if optional flag set.
        if treat_as_code is True:
            section = self.line_break + (" " * self.indent_width) + "@code" + section + (" " * self.indent_width) + "@endcode"
        
        # Append title string for section and return results.
        # >> NOTE:  Don't exceed more than a simple line break between the header and the contents 
        #           unless you want Doxygen to put indented contents in a 'box'.
        if section is not None and section != "":
            if str(title.strip()) != "":
                # Define base line break to use between title and section. Will be set to "" if no 
                # line break is desired.
                tmp_line_break = self.line_break
                
                # :: CHECK :: See if section starts with a bullet point symbol. If so, need to avoid 
                #               a blank line between it and the title it follows.
                if section.startswith(self.line_break):
                    # Obtain first word in sentence:
                    #  1. Remove first instance of 'line_break'.
                    #  2. Strip all whitespace and line breaks.
                    #  3. Split by whitespace characters.
                    #  4. Grab first word.
                    # Use this word as marker for possible 'bullet point number'.
                    first_word = section.replace(self.line_break, "", 1).strip().split(" ")[0]
                    if first_word in self.bullet_point_markers:
                        tmp_line_break = ""
                
                return "## " + str(title) + tmp_line_break + str(section) + self.section_break
            else:
                return str(section) + self.section_break
        else:
            return ""
    
    
    ################################################################################################
    # @brief        Structures a string representing how to invoke the current command from user 
    #               space.
    # 
    # @details      Implementation specifics left out [up to child class to define].
    # 
    # @param[in]    data_entry              Single entry from global 'self.data' (e.g. data[n]). The 
    #                                       values within this data set will be processed to create 
    #                                       a single-page summary of the data.
    # 
    # @warning      By default, this method is simply a placeholder. Child instances that inherit 
    #               this class must provide the implementation.
    ################################################################################################
    def _caller_syntax(self, data_entry):
        pass
    
    
    ################################################################################################
    # @brief        Parses the provided data and generates a page overview summary.
    # 
    # @details      Implementation specifics left out [up to child class to define].
    # 
    # @param[in]    data_entry              Single entry from global 'self.data' (e.g. data[n]). The 
    #                                       values within this data set will be processed to create 
    #                                       a single-page summary of the data.
    # 
    # @warning      By default, this method is simply a placeholder. Child instances that inherit 
    #               this class must provide the implementation.
    ################################################################################################
    def generate_page(self, data):
        pass
    
    
    ################################################################################################
    # @brief        Generates summary pages for the current class instance data set.
    # 
    # @details      Processes the global 'data' list and converts each entry in the list to a 
    #               single page entry. The formatting of each page is covered by invoking the 
    #               'self.generate_page()' method.
    # 
    # @note         It is not possible to add the page break before a new section as of the time of 
    #               writing this method. (Results in an 'unrecognized command' error in LaTeX 
    #               processing.)
    # 
    # @param[in]    header                  [OPTIONAL] Name of header to insert before any summary 
    #                                       information is generated.
    # @param[in]    trailing_page_break     [OPTIONAL] Flag indicating that there should always be 
    #                                       a page break inserted before a new page. Defaults to 
    #                                       'False'.
    # 
    # @returns      String containing full summary generated by processing the class instance 
    #               'data' variable.
    ################################################################################################
    def generate(self, header=None, trailing_page_break=False):
        # Define local variable(s) used in function.
        output                              = ""                                # String containing formatted output.
        
        # Loop through all entries in data list and create page summary.
        for entry in self.data:
            # Add page break IFF not first entry in output.
            if output != "":
                output += self.section_divider + self.page_break + self.paragraph_break
            output += self.generate_page(entry)
        
        # Prepend optional header.
        if header is not None:
            output = str(header) + (self.line_break * 2) + output
        
        # :: CHECK :: Ensure non-empty output before continuing. ::
        if len(output) == "":
            return None
        
        if trailing_page_break == True:
            output += self.page_break + self.paragraph_break
        
        return output




