####################################################################################################
# @brief        Provides classes and methods used to summarize comments within a source project 
#               which utilizes Doxygen. Summaries can then be used to generate markdown [.md] 
#               summaries.
# 
# @author       Michael Stephens (MCS)
# 
# @attention    DEPENDENCIES:
#                   * Python 2.7.X
####################################################################################################


####################################################################################################
## Import Modules
####################################################################################################
from __future__ import print_function                                           # Add support for update print function.
import os, sys                                                                  # Manipulate system-level functions.




####################################################################################################
# @class        SummarizeDoxgenComments
# 
# @brief        Provides methods for extracting and summarizing details on Doxygen comments within a 
#               source project.
# 
# @details      The main aim of this class to provide a consistent manner for generating summaries 
#               of comment blocks. This class provides methods for extracting and grouping related 
#               Doxygen tags. Theses summaries can then be used to generate summaries (e.g. via 
#               markdown [.md] files.
####################################################################################################
class SummarizeDoxgenComments(object):
    
    
    ################################################################################################
    # DEFINE CLASS GLOBALS [CONSTANTS, VARS, ETC.]
    ################################################################################################
    
    ## Path to root of project that will be processed.
    __dir_project           = ""
    
    ## List of subdirectories to exclude when processing a project.
    __dir_exclude_list      = []
    
    ## List of files to exclude when processing a project.
    #  [NOTE:  Must be full file name (not something like *.py).
    __file_exclude_list     = []
    
    ## Define supported Doxygen markers for tags.
    __doxygen_tag_markers   = [ "@", "\\" ]
    
    ## Details for supported languages that will be processed.
    #  Structured as a list of dictionaries. Where, each dictionary contains:
    #   * type                  == [REQUIRED] File type [description].
    #   * extensions            == [REQUIRED] List of file extensions associated with the language.
    #                                         
    #   * comment_block_start   == [REQUIRED] List of the possible first character sequences at the 
    #                                         start of a line [excluding whitespace] used to 
    #                                         indicate the start of a Doxygen comment block.
    #   * comment_block_lines   == [OPTIONAL] List of the possible character markers at the start of 
    #                                         each line within a comment block.
    #   * comment_block_end     == [OPTIONAL] List of the possible character sequences used to 
    #                                         indicate the end of a Doxygen comment block.
    __supported_languages   = [
                                { 
                                    "type":                 "C/C++",
                                    "extensions":           [ "h", "c", "cpp" ],
                                    "comment_block_start":  [ "///", "//!", "/**", "/*!" ],
                                    "comment_block_lines":  [ "///", "//!", "*"],
                                    "comment_block_end":    [ "*/" ]
                                }, {
                                    "type":                 "Python",
                                    "extensions":           ["py"],
                                    "comment_block_start":  [ "##", '"""' ],
                                    "comment_block_lines":  [ "#"],
                                    "comment_block_end":    None
                                }
                            ]
    
    ## Lenght of a tab character.
    __tab_length            = 4
    
    ## Define list used to track the output of the most recent query. By default, this variable will 
    #  contain the most recent data set from one of the available parse methods [cleaned at the 
    #  start of each parse method].It will contains a list of dictionaries where, each dictionary in 
    #  the list is contains summary lists of tag information. For example:
    #    * [
    #        * { 
    #            * brief:      [ "Exmple brief description." ],
    #            * retval:     [ "1 - 1st value.", "1 - 2nd value." ],
    #        * }, {
    #            * brief:      [ "Example brief description." ],
    #            * param[in]:  [ "param-01 ...", "param-02 ...", "param-03 ..." ],
    #            * param[out]: [ "param-04 ..." ],
    #            * returns:    [ "Description of returns" ]
    #        * }
    #    * ]
    __data                  = None
    
    
    ################################################################################################
    # @brief        Constructor for the class.
    # 
    # @param[in]    project_directory       Path to the root of the project you wish to process. 
    #                                       Typically, this is an absolute path.
    # @param[in]    dir_exclude_list        [OPTIONAL] List of subdirectories to bypass when 
    #                                       processing. Any subdirectory matching a name is this 
    #                                       list will be skipped [including nested levels].
    # @param[in]    file_exclude_list       [OPTIONAL] List of file names to skip when processing. 
    #                                       Must be a list of [complete] file names [partial match 
    #                                       not supported].
    # @param[in]    supported_languages     [OPTIONAL] Override for the default supported languages 
    #                                       structure. Must match the expected structure of:
    #                                       [{"type", "extensions", "comments"}].
    # @param[in]    tab_length              [OPTIONAL] Length of tab [\t] character used in text 
    #                                       being parsed. Defaults to '4'.
    # 
    # @note         By default, the 'supported_languages' will be set to the initial class 
    #               definition for '__supported_languages'.
    # 
    # @returns      Nothing, but will raise an exception if there is an error with the values 
    #               provided.
    ################################################################################################
    def __init__(self, project_directory, dir_exclude_list=None, file_exclude_list=None, supported_languages=None, tab_length=4):
        
        # Validate and update 'project_directory'
        if os.path.exists(project_directory):
            self.__dir_project = os.path.realpath(project_directory)
        else:
            raise ValueError("Provided project directory could not be found.")
        
        # Validate and update 'dir_exclude_list'.
        if dir_exclude_list is not None:
            if isinstance(dir_exclude_list, list):
                self.__dir_exclude_list = dir_exclude_list
            else:
                raise ValueError("Provided value for 'dir_exclude_list' must be a list.")
        
        # Validate and update 'file_exclude_list'.
        if file_exclude_list is not None:
            if isinstance(file_exclude_list, list):
                self.__file_exclude_list = file_exclude_list
            else:
                raise ValueError("Provided value for 'file_exclude_list' must be a list.")
        
        # Validate and update the 'supported_languages'.
        if supported_languages is not None:
            for lang in supported_languages:
                if len(lang) != len(self.__supported_languages[0]):
                    raise ValueError("Provided value for 'supported_languages' does not match required structure [invalid dictionary length for one or more languages].")
                for key in self.__supported_languages[0]:
                    if key not in lang:
                        raise ValueError("Provided value for 'supported_languages' does not match required structure [missing one or more dictionary parameters].")
            self.__supported_languages = supported_languages
        
        # Update 'tab_length' value.
        self.__tab_length = tab_length
    
    
    ################################################################################################
    # @brief        Validate the path and file name provided and returns the index into the language 
    #               array relevant for the current file.
    # 
    # @details      Function validates parameters by confirming the file:
    #                   1. Exists.
    #                   2. Does not match one of the excluded file names.
    #                   3. Does not reside within one of the excluded subdirectories.
    #                   4. Has a valid extension type.
    #               
    #               If the above checks pass, the function will return the index into the supported 
    #               languages list to caller.
    #               
    # @note         This could be two functions but mesh well enough together to be combined into 
    #               one.
    # 
    # @param[in]    fpath                   Path to directory containing file.
    # @param[in]    fname                   Name of file [no path info; with extension].
    # 
    # @retval       -4                      [ERROR] File not in an excluded directory, but extension 
    #                                       not supported.
    # 
    # @retval       -3                      [ERROR] File name is within the list of excluded file 
    #                                       names.
    # 
    # @retval       -2                      [ERROR] File resides in within an excluded directory.
    # @retval       -1                      [ERROR] File does not exist.
    # @retval       >=0                     [SUCCESS] Index into the class instances global variable 
    #                                       for 'supported languages'.
    ################################################################################################
    def _validate_path_and_language(self, path):
        
        # :: CHECK :: Validate file exists. ::
        if not os.path.exists(path):
            return -1
        
        # Break path out into components.
        fpath, fname = os.path.split(path)
        
        # Skip any files in the global 'directory exclude' list.
        if os.path.basename(fpath) in self.__dir_exclude_list:
            return -2
        
        # Skip any files in the global 'file exclude' list.
        if fname in self.__file_exclude_list:
            return -3
        
        # Loop until a valid match within the supported languages is found.
        index = 0
        for lang in self.__supported_languages:
            # Loop through supported extensions for the present language option.
            for ext in lang["extensions"]:
                if fname.endswith( "." + str(ext) ) is True:
                    return index
            
            # Increment counter used to reference which file this
            index += 1
        
        # Presume not-supported if reach this point and return an error.
        return -4
    
    
    ################################################################################################
    # @brief        Process project and summarize all appropriate comment blocks in a given file as 
    #               a list  of dictionaries.
    # 
    # @details      After validating the file exists and is supported, locates and summarizes all 
    #               comment blocks. Each comment block will be then be checked against the 
    #               [optional] list of 'required_tags'.
    #                                                                                               \n\n
    #               All valid comment blocks will summarized as a dictionary item and appended to 
    #               the global 'self.__data' entry (following the formatting described at the first 
    #               declaration for the class '__data' variable).
    #                                                                                               \n\n
    #               In addition to tag summaries, each dictionary will include '_target' and '_file' 
    #               entries to provide reference to where the summary originated from.
    # 
    # @note         The '_target' entry in each dictionary contains the entire line immediately 
    #               following the comment block. If the line is blank, it will contain the text 
    #               'None'. This is intended to provide a reference for function and class headers.
    # 
    # @note         The '_file' entry in each dictionary will contain the full path + file name for 
    #               the file from which the summary originates.
    # 
    # @note         Bullet point detection presently only supports bullet points NOT on the same 
    #               line as the Doxygen tag they are associated with [code begins searching for 
    #               bullet points on second line associated with any given tag].
    # 
    # @param[in]    fpath                   Path to file to process [for best results, use absolute 
    #                                       path].
    # @param[in]    required_tags           [OPTIONAL] Dictionary containing all required tags that 
    #                                       MUST be contained in a comment block for it to be 
    #                                       included. Each entry in the dictionary should be a list 
    #                                       of one or more valid values. If you only desire to 
    #                                       filter by tag type, use '*' as the only entry in the 
    #                                       list of options for the tag(s).
    # @param[in]    preserve_bp_indentation [OPTIONAL] True/false flag indicating if indentation 
    #                                       levels for bullet points should be preserved or ignored
    #                                       [if no value provided, set to True].
    # 
    # @warning      The parsing function detects the end of comment blocks by detecting one of the 
    #               following:
    #                   1. An empty line [length == zero (excluding whitespace)].
    #                   2. Line ends in one of the 'comment_block_end' 
    #                      characters.
    #                   3. Line is not one of the 'comment_block_lines' and the 
    #                      first character 'isalpha()'.
    #               
    #               As a consequence, the any C-style blocks MUST contain one of the designated 
    #               'comment_block_lines' entries (script specific requirement, Doxygen is more 
    #               generic and only requires proper ending markers).
    # 
    # @returns      A list of dictionaries containing all tag information contained within a comment 
    #               block (plus the additional '_file' and '_target' entries).
    #                                                                                               \n\n
    #               For additional details on the structure of the object returned, please see 
    #               'self.__data'.
    # 
    # @warning      In addition to returning the summarized data, the instance of the class global 
    #               variable 'self.__data' will be updated to contain the generated data.
    ################################################################################################
    def _parse(self, fpath, required_tags=None, preserve_bp_indentation=True):
        
        # Prep tracking variables.
        flag_within_comment_section         = False                             # Tracking flag used to indicate currently processing comment section.
        section_data                        = None                              # [TEMP] Variable used to record all tag related data in a comment section before formally adding it to the 'self.__data' variable.
        current_tag                         = None                              # [TEMP] Variable used to track which 'tag' is presently being processed for Doxygen summaries.
        flag_processing_custom_command      = False                             # Used to track when within a custom command "@<some-command>{ ... }". This allows:  (1) preserving of line breaks; and (2) removal aof '{' and '}'.
        flag_preserve_whitespace            = False                             # Used to track if whitespace should be preserved for comment while being extracted. Mainly used in:  {@verbatim, @code, ...}.
        bullet_points                       = []                                # List of all possible bullet point markers supported for detection during parsing.
        bullet_point_offset                 = 0                                 # Offset to start of bullet point on line. Used to detect nested bullet points levels.
        bullet_point_level                  = 0                                 # Tracking flag used to indicate current processing a bullet point and which level [0 == none, 1 == first level , etc.].
        
        # Add bullet point markers to tracking list.
        if preserve_bp_indentation is True:
            bullet_points.extend(["%d. " % x for x in range(1, 51)])
            bullet_points.append("#. ")
            bullet_points.append("- ")
            bullet_points.append("* ")
        
        # Verify file is supported before processing further.
        index_lang = self._validate_path_and_language(fpath)
        # >> If index below zero, file type not supported:
        if index_lang < 0:
            return
        # >> Create reference to relevant language information for 
        #    use during processing:
        else:
            language = self.__supported_languages[index_lang]
        
        # Notify user of what is being processed.
        print("> Processing '%s' file @ '%s' ..." % (language["type"], fpath.replace(self.__dir_project, ".")) )
        
        # Loop through lines in file and extract all relevant sections.
        with open(fpath, 'r') as fptr:
            
            line_break  = ""
            for line_raw in fptr:
                
                # Determine line break character to use/detect.
                if line_break == "":
                    if line_raw.endswith("\r\n"):
                        line_break = "\r\n"
                    elif line_raw.endswith("\n"):
                        line_break = "\n"
                    elif line_raw.endswith("\r"):
                        line_break = "\r"
                    else:
                        line_break = "\n"
                
                # Remove any line breaks and tabs from line (one tab == 4 spaces).
                line = line_raw.replace('\\t', (" " * self.__tab_length) ).replace('\t', (" " * self.__tab_length) ).replace('\\n', "").replace('\n', "").replace('\\r', "").replace('\r', "").strip()
                
                
                ############################################################################
                # The following logic handles the core process for interpreting comment 
                # sections in the file. The aim is to detect the start and stop of a comment 
                # block and to extract all possible tags within it. Following the comment 
                # block, the next line in the file will also be extracted. Similar tags will 
                # be grouped [e.g. all '@param[in]' types will be in one group but [in] vs 
                # [out] params will be separate groups.
                ############################################################################
                
                # >> Detect start of comment section:
                #    (1) Line starts with one of the possible 'comment_block_start' options 
                #        for the current language.
                #    **** AND ****
                #    (2) The line does NOT end with one of the 'comment_block_end' options.
                if flag_within_comment_section is False and line.startswith(tuple(language["comment_block_start"])):
                    
                    # Don't bother flagging section as comment block if one-line comment.
                    if language["comment_block_end"] != None and line.endswith(tuple(language["comment_block_end"])):
                        continue
                    
                    # Reset tracking data for future loop iterations.
                    flag_within_comment_section     = True
                    flag_processing_custom_command  = False
                    section_data                    = {}
                    continue
                
                # >> Detect end of comment section:
                #    (1) Empty line [length == zero].
                #    (2) Line ends in one of the 'comment_block_end' characters.
                #    (3) Line is not one of the 'comment_block_lines' and the first 
                #        character 'isalpha()'.
                elif (
                        flag_within_comment_section is True and (
                                len(line) == 0 
                            or  language["comment_block_end"] != None and line.endswith(tuple(language["comment_block_end"])) 
                            or  language["comment_block_lines"] != None and not line.startswith(tuple(language["comment_block_lines"])) and str(line[0]).isalpha()
                        )
                     ):
                    
                    # Clear flag indicating currently processing section.
                    flag_within_comment_section = False
                    
                    # Skip further processing IFF line contains the ending marker for a 
                    # comment block.
                    # >> NOTE:  Only want to skip loops if this line is the last line in a 
                    #           comment block. Clearing the flag will trigger the for-loop 
                    #           below to check for the '_target' line the comment block 
                    #           relates to. Don't want to mistakenly grab the marker 
                    #           indicating the comment block ending.
                    if language["comment_block_end"] != None and line.endswith(tuple(language["comment_block_end"])):
                        continue
                
                # >> Process tags within a comment section:
                elif flag_within_comment_section is True:
                    
                    # Clean up line by removing any optional comment line markers.
                    tmp = line
                    if language["comment_block_lines"] != None:
                        for marker in language["comment_block_lines"]:
                            if line.startswith(marker):
                                tmp  = tmp.replace(marker, "")          # Track what the line would look like if all instances removed [detect comment dividing lines that should be skipped].
                                line = line.replace(marker, "", 1)      # Update line by removing [only] the first instance of a comment marker.
                    
                    # :: CHECK :: Ensure the line contains more than comment markers. ::
                    if tmp.strip() == "":
                        # >> ADDENDUM:  Retain all whitespace when using the tag(s):  {@cmd_examples, @code}.
                        if flag_preserve_whitespace == True:
                            section_data[current_tag][ (len(section_data[current_tag]) - 1) ] = ("%s%s" % (section_data[current_tag][ (len(section_data[current_tag]) - 1) ], line_break ) )
                        continue
                    
                    # Remove and preceding whitespace and determine offset to first 
                    # non-space character in string.
                    tmp_len = len(line)
                    line    = line.strip()
                    offset  = tmp_len - len(line)
                    
                    # Replace any paragraph symbols with markdown header formatting [level 03].
                    # NOTE:  Purposely doing so after stripping any line endings (so break not lost) 
                    # and before checking tag markes (so paragraph tags don't incorrectly become 
                    # grouped separate from their current section).
                    if "@par" in line:
                        line = line.replace("@par ", "\n ### ", 1) + "\n"
                    
                    # Loop thought all potential markers looking for tags at the start of 
                    # the line.
                    flag_processed = False 
                    for tm in self.__doxygen_tag_markers:
                        
                        if line.startswith(tm):
                            
                            # Divide line up into tag and marker information.
                            tmp     = line.replace(tm, "", 1).split(" ")
                            tag     = tmp[0].strip()
                            comment = " ".join(tmp[1:]).strip()
                            
                            # Remove any trailing '{' from 'custom command'.
                            if tag.endswith("{"):
                                tag                             = tag[:-1]
                                flag_processing_custom_command  = True
                                if comment.endswith("}"):
                                    comment = comment[:-1].strip()
                            
                            # Create list for tag if first time encountering tag in section.
                            if not tag in section_data:
                                section_data[tag] = []
                            
                            # Append string and update tracking information.
                            section_data[tag].append( str(comment) )
                            current_tag                 = tag
                            flag_processed              = True
                            bullet_point_level          = 0
                            bullet_point_offset         = 0
                            
                            # Set flag indicating if whitespace should be preserved.
                            if(     (current_tag == "cmd_examples"  and "cmd_examples"  in section_data)
                                or  (current_tag == "code"          and "code"          in section_data) 
                                or  (current_tag == "verbatim"      and "verbatim"      in section_data)
                            ):
                                flag_preserve_whitespace = True
                            else:
                                flag_preserve_whitespace = False
                            
                            break
                    
                    # Append line to most recent tag insert IFF line not a new tag.
                    if flag_processed == False and current_tag != None:
                        # Prepend line break when preserving whitespace.
                        # NOTE:  This likely wind up with an extra line break preceding 
                        #        the example summary. This is desired as Doxygen output 
                        #        will preserve whitespace for any indented block of code 
                        #        that does not immediately follow a non-empty line.
                        if line != "" and flag_preserve_whitespace == True:
                            line = "\n" + line
                        
                        # :: CHECK :: See if processing the 'custom command' flag. ::
                        if flag_processing_custom_command is True:
                            # Clear flag if relevant.
                            if line.endswith("}"):
                                flag_processing_custom_command  = False
                                line                            = line[:-1].strip()
                        
                        # Process line either with or without bullet point checks [based 
                        # on flag passed to function].
                        if preserve_bp_indentation == True:
                            # Manage bullet point support.
                            # >> Line is a bullet point:
                            if line.startswith(tuple(bullet_points)):
                                # Update bullet point tracking variables.
                                if bullet_point_level > 0:
                                    bullet_point_level  = max( 1, bullet_point_level + int((offset - bullet_point_offset) / 4) )
                                else:
                                    bullet_point_level  = 1
                                #### 
                                bullet_point_offset     = offset
                                
                                # Insert appropriate indentation.
                                line                = (" " * self.__tab_length * bullet_point_level ) + line
                                
                                # Inserting two blank lines in a row will create odd Doxygen output 
                                # [don't want any blank lines between a paragraphs and bullet 
                                # points]. Checks below aimed at preventing that case.
                                if current_tag in section_data:
                                    tmp_prev_str = section_data[current_tag][ (len(section_data[current_tag]) - 1) ]
                                    if len(tmp_prev_str) > 0 and tmp_prev_str[-1] != "\n":
                                        line    = "\n" + line
                            
                            # >> Reset tracking for bullet points if:
                            #    0.  Bullet points are active [non-zero tracking variables].
                            #    A1. The line does not begin with a bullet point character.
                            #    A2. Is before the current bullet point indent level.
                            #    B1. The line is empty.
                            elif (bullet_point_level > 0 or bullet_point_offset != 0) and (offset < bullet_point_offset or line == ""):
                                # Reset tracking vars.
                                bullet_point_level  = 0
                                bullet_point_offset = 0
                                
                                # Prepend line break [line is first line after a bullet point section].
                                line                = "\n\n" + line
                            
                            # >> Not the start of a new bullet point or the 
                            #    clearing of bullet point tracking. Add a space 
                            #    separator between previous line and current line.
                            else:
                                line = " " + line
                        
                        # >> Insert extra space between lines if not preserving bullet numbering.
                        elif line != "":
                            line = " " + line
                        
                        # Append line.
                        section_data[current_tag][ (len(section_data[current_tag]) - 1) ] = ("%s%s" % (section_data[current_tag][ (len(section_data[current_tag]) - 1) ], str(line) ) )
                
                
                ############################################################################
                # Validate and copy over 'section_data' to class instance of '__data'. 
                # Additionally, record the current line as the '_target' and the file 
                # information as the '_file' for the comment.
                # --------------------------------------------------------------------------
                # NOTE: Make sure to do this outside of the above if statement tree.
                ############################################################################
                if flag_within_comment_section is False and section_data != None and len(section_data) > 0:
                    
                    # Record either 'None' or the full value of the current line. This will 
                    # allow the caller to know what function or block of code the tags 
                    # relate to.
                    if len(line) > 0:
                        section_data["_target"] = [str(line)]
                    else:
                        section_data["_target"] = [None]
                    
                    # Append file information.
                    section_data["_file"] = [fpath.replace(self.__dir_project, ".")]
                    
                    # Copy over 'section_data' if it contains the requested tag [or 
                    # automatically copy over if no tag was provided].
                    count_requirements_met = 0
                    if required_tags is not None:
                        for key, value_list in required_tags.items():
                            if key in section_data:
                                for val in value_list:
                                    if val in str(section_data[key]) or val == "*":
                                        # Increment count to indicate 
                                        # requirement validated and skip 
                                        # to next key in dictionary.
                                        count_requirements_met += 1
                                        break
                    
                    # >> Execute copy over of section data IFF validated.
                    if required_tags is None or count_requirements_met == len(required_tags.keys()):
                        self.__data.append(section_data)
                    
                    # Wipe 'section_data' so this block of code is not run again before 
                    # the next comment section is found.
                    section_data    = None
                    current_tag     = None
    
    
    ################################################################################################
    # @brief        Sorts all current 'self.__data[]["_target" values entries as if they were 
    #               functions.
    # 
    # @details      Loops through all 'self.__data' and attempt to locate a function name. All 
    #               entries are sorted alphabetically by 
    #               '_target' type in the following order:
    #                   1. All '_targets' with function names.
    #                   2. All '_targets' with non-empty, non-function values.
    #                   3. All '_targets' with empty [None] for their value.
    # 
    # @param[in]    data                    Optional data set to process. When no value provided, 
    #                                       uses the class instnace of 'self.__data'.
    # 
    # @warning      This function presumes all functions have the '(' character somewhere in their 
    #               definition. [Consequently:  (1) does not support sorting languages such as 
    #               assembly effectively; and (2) presumes all statements following comment blocks 
    #               with '(' are functions).]
    # 
    # @returns      A list of dictionaries sorted by the '_target' field.
    #                                                                                               \n\n
    #               For additional details on the structure of the object returned, please see 
    #               'self.__data'.
    # 
    # @warning      In addition to returning the summarized data, the instance of the class global 
    #               variable 'self.__data' will be updated to contain the generated data IFF no 
    #               'data' parameter is provided.
    ################################################################################################
    def sort_targets_as_functions(self, data=None):
        
        # Use class instance when 'data' is None.
        if data is None:
            flag_override = True
            data = self.__data
        else:
            flag_override = False
        
        # Prep tracking variables.
        targets     = []                                                        # [TEMP] Variable used for tracking and sorting all targets.
        tmp_data    = [None] * len(data)                                        # [TEMP] Sandbox variable used when shuffling the order of 'data'.
        
        # Return null [None] if data is empty.
        if len(data) == 0 or data is None:
            print("[WARNING] No data to sort for class instance.")
            self.__data = None
            return None
        
        # Loop through 'data' and create list of dictionaries summarizing available targets.
        cntr = 0
        for entry in data:
            
            # Process '_target' for function name based on if contains '(' character in line.
            # >> Null [None] indicates blank line and standalone target:
            if entry["_target"][0] is None:
                tmp = ""
                flag_is_function = False
            # >> IFF '(' located, it is a function:
            elif "(" in entry["_target"][0]:
                tmp = str(entry["_target"][0]).split("(")[0].strip()
                tmp = tmp.split(" ")[-1:][0].replace("*", "")
                flag_is_function = True
            # >> Presume plain text [not a function] if reach this point:
            else:
                tmp = str(entry["_target"][0])
                flag_is_function = False
            
            # Add entry to tracking list.
            targets.append( { "index": cntr, "is_func": flag_is_function, "target": tmp } )
            
            # Increment index counter.
            cntr += 1
        
        # Sort dictionaries.
        # >> NOTE:  This will result in all 'non-functions' preceding functions. We will want to 
        #           swap this behavior.
        targets = sorted(targets, key=lambda k: k["target"].lower())
        targets = sorted(targets, key=lambda k: k["is_func"])
        
        # Determine how many functionss and non-functions were located.
        numb_total      = len(targets)
        numb_functions  = sum(1 for k in targets if k.get('is_func') == True)
        numb_null       = sum(1 for k in targets if k.get('is_func') == False and k.get('target') == "")
        numb_remainder  = numb_total - numb_functions - numb_null
        
        # Use targets to reshuffle 'data'.
        cntr = 0
        for t in targets:
            # >> Insert targets == functions at start of list:
            if t["is_func"] is True:
                tmp_data[ (cntr - (numb_null + numb_remainder)) ] = data[int(t["index"])]
            # >> Insert non-empty targets just after functions:
            elif t["target"] != "":
                tmp_data[ (cntr + numb_functions - numb_null) ] = data[int(t["index"])]
            # >> Insert empty targets at end of list:
            else:
                tmp_data[ (cntr + numb_functions + numb_remainder) ] = data[int(t["index"])]
            cntr += 1
        
        # Update class tracking and return sorted data.
        if flag_override == True:
            self.__data = tmp_data
            return self.__data
        else:
            return tmp_data
    
    
    ################################################################################################
    # @brief        Wrapper method to internal parser method to summarize all files within the base
    #               directory for the instance of this class [base set during instantiation].
    # 
    # @details      Wipes the global 'self.__data' list and loops through all files in the project 
    #               path that pass the filter settings defined when the class instance was 
    #               generated.
    #                                                                                               \n\n
    #               For each file, the internal parsing method is invoked for processing.
    # 
    # @note         Please see the main 'self._parse()' method for full implementation details on 
    #               parsing a file.
    # 
    # @param[in]    sort_by_func_name       [OPTIONAL] True/false flag indicating if the generated 
    #                                       summary should be sorted by target value as if each 
    #                                       target entry is a function name [if no value provided, 
    #                                       set to True].
    # @param        required_tags           [OPTIONAL] Dictionary containing all required tags that 
    #                                       MUST be contained in a comment block for it to be 
    #                                       included. Each entry in the dictionary should be a list 
    #                                       of one or more valid values. If you only desire to 
    #                                       filter by tag type, use '*' as the only entry in the 
    #                                       list of options for the tag(s).
    # @param[in]    preserve_bp_indentation [OPTIONAL] True/false flag indicating if indentation 
    #                                       levels for bullet points should be preserved or ignored
    #                                       [if no value provided, set to True].
    # 
    # @returns      A list of dictionaries containing all tag information contained within a comment 
    #               block (plus the additional '_file' and '_target' entries).
    #                                                                                               \n\n
    #               For additional details on the structure of the object returned, please see 
    #               'self.__data'.
    # 
    # @warning      In addition to returning the summarized data, the instance of the class global 
    #               variable 'self.__data' will be updated to contain the generated data.
    ################################################################################################
    def parse_dir(self, sort_by_func_name=True, required_tags=None, preserve_bp_indentation=True):
        
        # Prep tracking variables.
        self.__data                         = []                                # Clear global [class instance] of data.
        
        # Loop through all files in root of the directory for the project and summarize those 
        # containing the list of 'required tags'.
        for root, dirs, files in os.walk(self.__dir_project, topdown=True):
            for f in files:
                self._parse(os.path.realpath(os.path.join(root, f)), required_tags, preserve_bp_indentation)
        
        # Set class instance of '__data' to null [None] if no valid sections.
        if len(self.__data) == 0:
            self.__data = None
        
        # Return data [sorted or non sorted based on function parameter].
        if sort_by_func_name == True and self.__data != None:
            return self.sort_targets_as_functions()
        else:
            return self.__data
    
    
    ################################################################################################
    # @brief        Wrapper method to internal parser method to summarize the given file within the 
    #               base directory for the instance of this class [base set during instantiation].
    # 
    # @details      Wipes the global 'self.__data' list and attempt to process the provided fname 
    #               relative to the base path defined when the class instance was generated.
    # 
    # @note         Please see the main 'self._parse()' method for full implementation details on 
    #               parsing a file.
    # 
    # @param[in]    fname                   Name of file within the current 'self.__dir_project' to 
    #                                       process. File is located by completing an 
    #                                       'os.path.join()' with the base directory.
    # @param[in]    sort_by_func_name       [OPTIONAL] True/false flag indicating if the generated 
    #                                       summary should be sorted by target value as if each 
    #                                       target entry is a function name [if no value provided, 
    #                                       set to True].
    # @param[in]    required_tags           [OPTIONAL] Dictionary containing all required tags that 
    #                                       MUST be contained in a comment block for it to be 
    #                                       included. Each entry in the dictionary should be a list 
    #                                       of one or more valid values. If you only desire to 
    #                                       filter by tag type, use '*' as the only entry in the 
    #                                       list of options for the tag(s).
    # @param[in]    preserve_bp_indentation [OPTIONAL] True/false flag indicating if indentation 
    #                                       levels for bullet points should be preserved or ignored
    #                                       [if no value provided, set to True].
    # 
    # @returns      A list of dictionaries containing all tag information contained within a comment 
    #               block (plus the additional '_file' and '_target' entries).
    #                                                                                               \n\n
    #               For additional details on the structure of the object returned, please see 
    #               'self.__data'.
    # 
    # @warning      In addition to returning the summarized data, the instance of the class global 
    #               variable 'self.__data' will be updated to contain the generated data.
    ################################################################################################
    def parse_file(self, fname, sort_by_func_name=True, required_tags=None, preserve_bp_indentation=True):
        
        # Prep tracking variables.
        self.__data                         = []                                # Clear global [class instance] of data.
        
        # Summarize file [gather all sections containing the 'required tags'].
        self._parse(os.path.realpath(os.path.join(self.__dir_project, fname)), required_tags, preserve_bp_indentation)
        
        # Set class instance of '__data' to null [None] if no valid sections.
        if len(self.__data) == 0:
            self.__data = None
        
        # Return data [sorted or non sorted based on function parameter].
        if sort_by_func_name == True and self.__data != None:
            return self.sort_targets_as_functions()
        else:
            return self.__data
    
    
    ################################################################################################
    # @brief        Wrapper to 'self.parse_dir()' method to look explicitly for comment sections 
    #               containing '[at]ingroup' tags.
    # 
    # @details      Passes the [single] name provided for an [at]ingroup tag value to the main 
    #               parser function. Serves mainly as an abstraction for the user.
    # 
    # @note         For the purposes of this function, a 'Doxygen group' is:
    #                   * Any function header which contains an '[at]ingroup' tag.
    # 
    # @warning      Only supports looking for a single '[at]ingroup' tag. For more complex 
    #               characterizations, please use the 'self.parse()' method directly.
    #               
    # @param[in]    parse_by_ingroup        [OPTIONAL] String containing the value required for all 
    #                                       [at]ingroup Doxygen tags. When left blank, will still  
    #                                       search for '[at]ingroup', but will accept any tag value.
    # 
    # @returns      Passes value returned from 'self.parse()' back to caller.
    ################################################################################################
    def parse_dir_by_ingroup(self, group_name="*"):
        return self.parse_dir(required_tags={"ingroup": [str(group_name)] }, sort_by_func_name=True)
    
    
    ################################################################################################
    # @brief        Wrapper to 'self.parse_file()' method to look explicitly for comment sections 
    #               containing '[at]ingroup' tags.
    # 
    # @details      Passes the [single] name provided for an [at]ingroup tag value to the main 
    #               parser function. Serves mainly as an abstraction for the user.
    # 
    # @note         For the purposes of this function, a 'Doxygen group' is:
    #                   * Any function header which contains an '[at]ingroup' tag.
    # 
    # @warning      Only supports looking for a single '[at]ingroup' tag. For more complex 
    #               characterizations, please use the 'self.parse()' method directly.
    #               
    # @param[in]    parse_by_ingroup        [OPTIONAL] String containing the value required for all 
    #                                       [at]ingroup Doxygen tags. When left blank, will still  
    #                                       search for '[at]ingroup', but will accept any tag value.
    # 
    # @returns      Passes value returned from 'self.parse()' back to caller.
    ################################################################################################
    def parse_file_by_ingroup(self, fpath, group_name="*"):
        return self.parse_file(fpath, required_tags={"ingroup": [str(group_name)] }, sort_by_func_name=True)




