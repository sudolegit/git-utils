####################################################################################################
# @brief        Provides classes and methods related to project file manipulation.
# 
# @author       Michael Stephens (MCS)
# 
# @attention    DEPENDENCIES:
#                   * Python 2.7.X
####################################################################################################


####################################################################################################
## Import Modules
####################################################################################################
import os


####################################################################################################
# @brief            Locates the directory above the desired 'base_path' containing a <.git> 
#                   directory.
# 
# @details          Searches in reverse from the requested 'base_path' until a directory is found 
#                   containing a <.git> directory.
# 
# @param[in]        base_path               [OPTIONAL] Path to begin the search. Defaults to the 
#                                           path of the directory containing this script.
# 
# @warning          This is a very targeted search. Do NOT run this function if there is not a 
#                   <.git> directory.
# 
# @retval           Path-String             String containing absolute path to root directory for 
#                                           project.
# #retval           None                    Returned if invalid parameters provided or no <.git> 
#                                           directory is located before reaching the top level 
#                                           directory available.
#####################################################################################################
def git_root(base_path=None):
    
    # Note the current working directory so it can be restored when the function exits.
    path_orig = os.getcwd()
    
    # Change to a base working directory before the while loop begins searching upwards.
    # >> Override provided:
    if base_path is not None:
        if os.path.isdir(base_path):
            os.chdir(base_path)
        else:
            return None
    # >> No override, default at directory containing this script [presume it is within the 
    #    project]:
    else:
        os.chdir(os.path.dirname(os.path.realpath(__file__)))
    
    # Loop until a <.git> directory is located.
    prev_path = os.getcwd()
    while True:
        # >> Break from path if directory contains a <.git> directory.
        if os.path.isdir(".git"):
            prev_path = os.getcwd()
            break
        # >> If no <.git> directory, step up one level and try again on next loop iteration.
        else:
            # Move upwards by one directory level.
            prev_path = os.getcwd()
            os.chdir("..")
            
            # >> Error:  Not possible to go any higher in the path. Recover original path and 
            #            return 'None' to indicate an error.
            if os.getcwd() == prev_path:
                os.chdir(path_orig)
                return None
    
    # Return to original path value and return string containing path.
    os.chdir(path_orig)
    return prev_path


