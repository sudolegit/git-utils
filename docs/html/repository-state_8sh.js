var repository_state_8sh =
[
    [ "number_of_modified_files", "repository-state_8sh.html#ad98b7845fee2fb6a850bca5b659bcb70", null ],
    [ "number_of_staged_files", "repository-state_8sh.html#a1b0c7299102e0696e6371ece13529ad8", null ],
    [ "number_of_untracked_files", "repository-state_8sh.html#a2e1be7f40d0d7e6416b082cc27ecde67", null ],
    [ "repository_head_is_detached", "repository-state_8sh.html#aafa2e87eea75d215934161ad0aaa1f5b", null ],
    [ "repository_state_is_clean", "repository-state_8sh.html#a1a34f7b67326ab4204cfe2057b19aff5", null ],
    [ "repository_state_is_clean_enough", "repository-state_8sh.html#ad21c68e839cc7d97cd6610afd2efecb9", null ]
];