var branches_8sh =
[
    [ "checkout_branch", "branches_8sh.html#a63360a2da61ba585b79c4df3e42ecd90", null ],
    [ "checkout_most_recently_active_branch", "branches_8sh.html#a80be517fd5e553277928022c80cc953b", null ],
    [ "get_name_of_current_branch", "branches_8sh.html#a61619562d319a188e633a987a98efb59", null ],
    [ "local_branch_exists", "branches_8sh.html#a5ee816aca2fdc30e0b7ecb3c37b969e5", null ],
    [ "remote_branch_exists", "branches_8sh.html#a4e3fe7a5de16c7f7a31f12ea9c896d0a", null ]
];