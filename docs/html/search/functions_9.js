var searchData=
[
  ['navigate_5fto_5ffallback_5fdirectory',['navigate_to_fallback_directory',['../general_8sh.html#abf0ec9e9afef3d02469c603cf5d46a8b',1,'general.sh']]],
  ['navigate_5fto_5fworking_5fdirectory',['navigate_to_working_directory',['../general_8sh.html#a6f009abea91024e41d1b27da1472335b',1,'general.sh']]],
  ['number_5fof_5fmodified_5ffiles',['number_of_modified_files',['../repository-state_8sh.html#ad98b7845fee2fb6a850bca5b659bcb70',1,'repository-state.sh']]],
  ['number_5fof_5fstaged_5ffiles',['number_of_staged_files',['../repository-state_8sh.html#a1b0c7299102e0696e6371ece13529ad8',1,'repository-state.sh']]],
  ['number_5fof_5funtracked_5ffiles',['number_of_untracked_files',['../repository-state_8sh.html#a2e1be7f40d0d7e6416b082cc27ecde67',1,'repository-state.sh']]]
];
