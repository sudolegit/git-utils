var searchData=
[
  ['checkout_5fbranch',['checkout_branch',['../branches_8sh.html#a63360a2da61ba585b79c4df3e42ecd90',1,'branches.sh']]],
  ['checkout_5fmost_5frecently_5factive_5fbranch',['checkout_most_recently_active_branch',['../branches_8sh.html#a80be517fd5e553277928022c80cc953b',1,'branches.sh']]],
  ['claim',['claim',['../_l_i_c_e_n_s_e_8_t_x_t.html#a416ac50731e1e01973baec91961681fd',1,'claim(s):&#160;LICENSE.TXT'],['../_l_i_c_e_n_s_e_8_t_x_t.html#a9e05461d4dbeafe80635795dd88db7d6',1,'claim(excluding declaratory judgment actions, counter-claims, and cross-claims) alleging that a Contributor Version directly or indirectly infringes any patent:&#160;LICENSE.TXT']]],
  ['clean_5fstate_5fmode_5fis_5fenabled',['clean_state_mode_is_enabled',['../runtime-flags_8sh.html#a5c1e79b61405e4cf4666aee4de3186e5',1,'runtime-flags.sh']]],
  ['contributor',['Contributor',['../_l_i_c_e_n_s_e_8_t_x_t.html#a49f687cd2348b60f7bc2b16491cd48b7',1,'LICENSE.TXT']]],
  ['current_5fdirectory_5fis_5fgit_5frepository',['current_directory_is_git_repository',['../repository-checks_8sh.html#a5223b29e672bbedde2f1af14e48054c2',1,'repository-checks.sh']]],
  ['current_5fdirectory_5fis_5frepository_5froot',['current_directory_is_repository_root',['../repository-checks_8sh.html#a3d8a5f45f0974f2af1fd0b26da331874',1,'repository-checks.sh']]]
];
