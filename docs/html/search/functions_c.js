var searchData=
[
  ['recursive_5fmode_5fis_5fenabled',['recursive_mode_is_enabled',['../runtime-flags_8sh.html#a4f5d5184f470e7179bdb9829f3084acb',1,'runtime-flags.sh']]],
  ['reinstated',['reinstated',['../_l_i_c_e_n_s_e_8_t_x_t.html#aab85b168d5e15f90fc0ec6154eb20780',1,'LICENSE.TXT']]],
  ['remote_5faccessible',['remote_accessible',['../remotes_8sh.html#aeeac118ae6adc99193bb1c0fd309ed6f',1,'remotes.sh']]],
  ['remote_5fbranch_5fexists',['remote_branch_exists',['../branches_8sh.html#a4e3fe7a5de16c7f7a31f12ea9c896d0a',1,'branches.sh']]],
  ['remote_5fexists',['remote_exists',['../remotes_8sh.html#a998841e6e97f80685b07425c1105ff1f',1,'remotes.sh']]],
  ['repository_5fhead_5fis_5fdetached',['repository_head_is_detached',['../repository-state_8sh.html#aafa2e87eea75d215934161ad0aaa1f5b',1,'repository-state.sh']]],
  ['repository_5fstate_5fis_5fclean',['repository_state_is_clean',['../repository-state_8sh.html#a1a34f7b67326ab4204cfe2057b19aff5',1,'repository-state.sh']]],
  ['repository_5fstate_5fis_5fclean_5fenough',['repository_state_is_clean_enough',['../repository-state_8sh.html#ad21c68e839cc7d97cd6610afd2efecb9',1,'repository-state.sh']]],
  ['return_5ffrom_5ffunction',['return_from_function',['../general_8sh.html#a7e7b08aa6a60f434286eedb4cd281ef1',1,'general.sh']]]
];
