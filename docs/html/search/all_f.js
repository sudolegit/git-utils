var searchData=
[
  ['of',['of',['../_l_i_c_e_n_s_e_8_t_x_t.html#af9b88b3996243ec68fc248bf413cf762',1,'LICENSE.TXT']]],
  ['offer',['offer',['../_l_i_c_e_n_s_e_8_t_x_t.html#a313ec001b4c4424e55b7e049786753da',1,'LICENSE.TXT']]],
  ['option',['option',['../_l_i_c_e_n_s_e_8_t_x_t.html#aac061175442665218f955ef99e0b348c',1,'LICENSE.TXT']]],
  ['or',['or',['../_l_i_c_e_n_s_e_8_t_x_t.html#a441b1ff4b3c7a2591742f228eff4d2da',1,'or(b) ownership of more than fifty percent(50%) of the outstanding shares or beneficial ownership of such entity. 2. License Grants and Conditions 2.1. Grants Each Contributor hereby grants You a world-wide:&#160;LICENSE.TXT'],['../_l_i_c_e_n_s_e_8_t_x_t.html#a95f5d627b0f84138cfc5262bcc394813',1,'or(ii) the combination of its Contributions with other software(except as part of its Contributor Version):&#160;LICENSE.TXT']]],
  ['order',['order',['../_l_i_c_e_n_s_e_8_t_x_t.html#ad36ee2c2c777074da83c20d1df62be0e',1,'LICENSE.TXT']]],
  ['others',['others',['../_l_i_c_e_n_s_e_8_t_x_t.html#afe5c92df72f03d445909e9eac025a4da',1,'LICENSE.TXT']]],
  ['otherwise',['otherwise',['../_l_i_c_e_n_s_e_8_t_x_t.html#aa35636e8f4329859f8fc41d1fff15a13',1,'LICENSE.TXT']]],
  ['override_5fmode_5fis_5fenabled',['override_mode_is_enabled',['../runtime-flags_8sh.html#a4508c6c66ca595016b55311158cea3bd',1,'runtime-flags.sh']]]
];
