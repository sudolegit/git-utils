var searchData=
[
  ['changelog_2emd',['Changelog.md',['../_changelog_8md.html',1,'']]],
  ['checkout_5fbranch',['checkout_branch',['../branches_8sh.html#a63360a2da61ba585b79c4df3e42ecd90',1,'branches.sh']]],
  ['checkout_5fmost_5frecently_5factive_5fbranch',['checkout_most_recently_active_branch',['../branches_8sh.html#a80be517fd5e553277928022c80cc953b',1,'branches.sh']]],
  ['choice',['choice',['../_l_i_c_e_n_s_e_8_t_x_t.html#a29786b07b17013dc9e636e94fdedcefa',1,'LICENSE.TXT']]],
  ['claim',['claim',['../_l_i_c_e_n_s_e_8_t_x_t.html#a416ac50731e1e01973baec91961681fd',1,'claim(s):&#160;LICENSE.TXT'],['../_l_i_c_e_n_s_e_8_t_x_t.html#a9e05461d4dbeafe80635795dd88db7d6',1,'claim(excluding declaratory judgment actions, counter-claims, and cross-claims) alleging that a Contributor Version directly or indirectly infringes any patent:&#160;LICENSE.TXT']]],
  ['claims',['claims',['../_l_i_c_e_n_s_e_8_t_x_t.html#a1ed9524ded2a969c37e3bc915ca388a4',1,'LICENSE.TXT']]],
  ['clean_5fstate_5fmode_5fis_5fenabled',['clean_state_mode_is_enabled',['../runtime-flags_8sh.html#a5c1e79b61405e4cf4666aee4de3186e5',1,'runtime-flags.sh']]],
  ['compliant',['compliant',['../_l_i_c_e_n_s_e_8_t_x_t.html#a625419a48763c9f98fa1f5e36b83459c',1,'LICENSE.TXT']]],
  ['contract',['contract',['../_l_i_c_e_n_s_e_8_t_x_t.html#a92a1d14f33caa7d477361883eb5ef2cd',1,'LICENSE.TXT']]],
  ['contribute',['contribute',['../_l_i_c_e_n_s_e_8_t_x_t.html#a25d3b4bde1e8b746ac51f00a671df689',1,'LICENSE.TXT']]],
  ['contributions',['Contributions',['../_l_i_c_e_n_s_e_8_t_x_t.html#a024c6e386bf7e756535c87ed769cbee7',1,'LICENSE.TXT']]],
  ['contributor',['Contributor',['../_l_i_c_e_n_s_e_8_t_x_t.html#a6a300c8951bc765e76fdb0ee591a7165',1,'Contributor():&#160;LICENSE.TXT'],['../_l_i_c_e_n_s_e_8_t_x_t.html#a49f687cd2348b60f7bc2b16491cd48b7',1,'Contributor(except as may be necessary to comply with the notice requirements in Section 3.4). 2.4. Subsequent Licenses No Contributor makes additional grants as a result of Your choice to distribute the Covered Software under a subsequent version of this License(see Section 10.2) or under the terms of a Secondary License(if permitted under the terms of Section 3.3). 2.5. Representation Each Contributor represents that the Contributor believes its Contributions are its original creation(s) or it has sufficient rights to grant the rights to its Contributions conveyed by this License. 2.6. Fair Use This License is not intended to limit any rights You have under applicable copyright doctrines of fair use:&#160;LICENSE.TXT']]],
  ['controls',['controls',['../_l_i_c_e_n_s_e_8_t_x_t.html#abee1d0a3801037ae56ec122ae4d81745',1,'LICENSE.TXT']]],
  ['creates',['creates',['../_l_i_c_e_n_s_e_8_t_x_t.html#a8f14e27887dfabd5e3e199a7c74209a3',1,'LICENSE.TXT']]],
  ['current_5fdirectory_5fis_5fgit_5frepository',['current_directory_is_git_repository',['../repository-checks_8sh.html#a5223b29e672bbedde2f1af14e48054c2',1,'repository-checks.sh']]],
  ['current_5fdirectory_5fis_5frepository_5froot',['current_directory_is_repository_root',['../repository-checks_8sh.html#a3d8a5f45f0974f2af1fd0b26da331874',1,'repository-checks.sh']]],
  ['changelog',['Changelog',['../md__home_sudolegit__8git-utils__changelog.html',1,'']]]
];
