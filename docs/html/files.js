var files =
[
    [ "branches.sh", "branches_8sh.html", "branches_8sh" ],
    [ "general.sh", "general_8sh.html", "general_8sh" ],
    [ "git-co", "git-co.html", null ],
    [ "git-execute", "git-execute.html", null ],
    [ "git-next", "git-next.html", null ],
    [ "git-previous", "git-previous.html", null ],
    [ "git-set-protocol", "git-set-protocol.html", null ],
    [ "git-walk", "git-walk.html", null ],
    [ "LICENSE.TXT", "_l_i_c_e_n_s_e_8_t_x_t.html", "_l_i_c_e_n_s_e_8_t_x_t" ],
    [ "navigate-history.sh", "navigate-history_8sh.html", "navigate-history_8sh" ],
    [ "remotes.sh", "remotes_8sh.html", "remotes_8sh" ],
    [ "repository-checks.sh", "repository-checks_8sh.html", "repository-checks_8sh" ],
    [ "repository-state.sh", "repository-state_8sh.html", "repository-state_8sh" ],
    [ "runtime-flags.sh", "runtime-flags_8sh.html", "runtime-flags_8sh" ],
    [ "source-all.sh", "source-all_8sh.html", null ],
    [ "terminal-output.sh", "terminal-output_8sh.html", "terminal-output_8sh" ]
];