var NAVTREE =
[
  [ "Git Utils", "index.html", [
    [ "Changelog", "md__home_sudolegit__8git-utils__changelog.html", null ],
    [ "Background", "md__home_sudolegit__8git-utils__r_e_a_d_m_e.html", null ],
    [ "Files", null, [
      [ "File List", "files.html", "files" ],
      [ "File Members", "globals.html", [
        [ "All", "globals.html", null ],
        [ "Functions", "globals_func.html", null ],
        [ "Variables", "globals_vars.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"_l_i_c_e_n_s_e_8_t_x_t.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';