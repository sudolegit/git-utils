# Changelog

----------------------------------------------------------------------------------------------------
## Release v1.2.0 - [2022-07-28]
1.  Adding support for the following git commands:
    -   git-co          [v1.0.0]
    -   git-next        [v1.0.0]
    -   git-previous    [v1.0.0]
    -   git-walk        [v1.0.0]
2.  Library changes in this release:
    -   branches.sh:
        -   *New library file.*
        -   Provides functionality for interacting with branches for:
            -   Checking if a local or remote branch exists.
            -   Getting the name of the active branch.
            -   Checking out a specific branch.
            -   Checking out the most recently active branch.
    -   navigate-history.sh:
        -   *New library file.*
        -   Provides functionality for interacting with commit history for:
            -   Walking up or down the git history (changing the active [local] branch pointer).
    -   remotes.sh:
        -   New function for checking if a remote exists.
        -   New function for checking if a remote is accessible.
    -   repository-state.sh:
        -   *New library file.*
        -   Provides functionality for checking the current state of a repository for:
            -   Determining if a repository is in a 'clean' or 'clean enough' state.
            -   Determining the number of modified, staged, or untracked files.
            -   Determining if the repository is in a 'detached HEAD' state.
    -   runtime-flags.sh:
        -   Expanding runtime flags to include 'clean state' and 'override' modes.
3.  Aliases:
    -   Dropping support for 'co' now that the corresponding 'git-co' command has been created.


----------------------------------------------------------------------------------------------------
## Release v1.1.0 - [2022-03-13]
1.  Adding support for the following git commands:
    -   git-execute     [v1.0.0]
    -   git-snapshot    [v1.0.0]
2.  Adding the concept of library support for git commands. Library support allows main commands 
    to share functions and reduce code reduncancy. Library functions added in this release:
    -   general.sh:
        -   ensure_in_repository_root()
        -   execute_command_on_git_repositories()
        -   exit_script()
        -   navigate_to_fallback_directory()
        -   navigate_to_working_directory()
        -   return_from_function()
        -   set_fallback_directory()
        -   set_working_directory()
    -   remotes.sh:
        -   __translate_remote_url()
        -   execute_command_on_git_repository_remotes()
        -   set_remote_protocol()
    -   repository-checks.sh:
        -   current_directory_is_git_repository()
        -   current_directory_is_repository_root()
    -   runtime-flags.sh:
        -   dry_run_mode_is_enabled()
        -   enable_dry_run_mode()
        -   enable_include_nested_mode()
        -   enable_recursive_mode()
        -   enable_verbose_mode()
        -   include_nested_mode_is_enabled()
        -   recursive_mode_is_enabled()
        -   verbose_mode_is_enabled()
    -   terminal-output.sh:
        -   print_message()
        -   print_verbose_message()


----------------------------------------------------------------------------------------------------
## Release v1.0.0 - [2022-02-17]
1. Capturing all development as of this moment as the first release.
    -   This contains years of development and mainly saving as a snapshot 
        before diving into the next round of development.


